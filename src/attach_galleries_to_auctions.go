package main
import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
	"github.com/coopernurse/gorp"
	"strconv"
	"sellerfoxPkg"
	"time"
	"os"
)

func main() {
//============================================== connect to DB and set up gorp ==============================================//
	//connect to DB
	db, err := sql.Open("mysql", "root:@tcp(localhost:3306)/sellerfox?charset=utf8")
	if err != nil {
		fmt.Println("Failed to connect", err)
		return
	}
	defer db.Close()

	//create database map to start using gorp
	dbmap := &gorp.DbMap{Db: db, Dialect: gorp.MySQLDialect{"InnoDB", "UTF8"}}
	//add structs as tables into dmap
	dbmap.AddTable(sellerfoxPkg.Sf_contract{}).SetKeys(true, "sf_login_id")
	dbmap.AddTable(sellerfoxPkg.Sfg_attach_gallery_queue{}).SetKeys(false)
	dbmap.AddTable(sellerfoxPkg.Sfg_auctions{}).SetKeys(false)
	dbmap.AddTable(sellerfoxPkg.Sfg_settings{}).SetKeys(false)
	dbmap.AddTable(sellerfoxPkg.Single_row_gallery_view{}).SetKeys(false)
	dbmap.AddTable(sellerfoxPkg.Sfg_gallery{}).SetKeys(false)
//====================================================== start logging ======================================================//
	//get current time
	t := time.Now().Local()
	file_address := sellerfoxPkg.File_address+"/ebay_attach_"+string(t.Format("2006_01_02"))+".log"
	log_file, _ := os.OpenFile(file_address, os.O_APPEND|os.O_WRONLY, 0600)
	if sellerfoxPkg.Debug == true {
		//create or open today's file
		if _, err := os.Stat(file_address); err == nil {
			fmt.Println("today's debug file exist.",file_address)
		} else {
			fmt.Println("creating today's debug file", file_address)
			os.Create(file_address)
		}

		log_file, err = os.OpenFile(file_address, os.O_APPEND|os.O_WRONLY, 0600)
		if err != nil {
			fmt.Println("error writing to file 0:",err)
		}
		defer log_file.Close()

		if _, err = log_file.WriteString("====================================\r\nStarted at "+string(t.Format("2006-01-02 15:04:05"))+"\r\n\r\n\r\n"); err != nil {
			fmt.Println("error writing to file 1:",err)
		}
	}
	//log avoid_duplicates value
	if sellerfoxPkg.Debug == true {
		if _, err = log_file.WriteString("Default Avoid_duplicates value is "+strconv.FormatBool(sellerfoxPkg.Avoid_duplicates)+"\r\n"); err != nil { fmt.Println("error writing to file 2:", err) }
	}
//=================================== execute sql query and check if the result is empty ====================================//
	//limit the amount of users being worked with during script's execution
	const Users_count int = 150
	//limit the amount of attached galleries per user during script's execution
	const Auction_count int = 10

	//SQL query text. it gets a list of all users(ids) that want to publish galleries
	query_text := "SELECT sf_login_id "
	query_text += "FROM sellerfox.sf_contract "
	query_text += "WHERE sellerfox.sf_contract.gekuendiged = 0 "
	query_text += "AND sellerfox.sf_contract.not_attach_auctions = 1 "
	query_text += "ORDER BY RAND() limit " + strconv.Itoa(Users_count)

	//array of table rows
	var client_ids []int
	//fill array with sql query results
	_, err = dbmap.Select(&client_ids, query_text)
	if err != nil {
		fmt.Println("SQL query error. ", err)
		return
	}

	if len(client_ids) == 0 {
		fmt.Println("There are no clients that want to publish galleries.")
		//log
		if sellerfoxPkg.Debug == true {
			if _, err = log_file.WriteString("There are no clients that want to attach galleries.\r\n"); err != nil { fmt.Println("error writing to file 3:", err) }
		}
	} else {
//=========================== if sql result is not empty then publish galleries for each customer ===========================//
		//log
		if sellerfoxPkg.Debug == true {
			if _, err = log_file.WriteString("Fetching clients that want to attach galleries.\r\n"); err != nil { fmt.Println("error writing to file 4:", err) }
		}
		//fmt.Println(client_ids) //delete later
		//for each client_id in client_ids
		for _, client_id := range client_ids {
			//log
			if _, err = log_file.WriteString("Working with client id"+strconv.Itoa(client_id)+".\r\n"); err != nil { fmt.Println("error writing to file 5:",err) }
			//generate client's individual DB name
			client_db := "sellerfox_" + sellerfoxPkg.Add_zeros(client_id) //sellerfoxPkg.Add_zeros(client_id)
			//fmt.Println(client_db," ") //delete later

			//attached galleries counter(should not get higher than than auction_count)
			gallery_counter := 0;
			//infinite cycle. here we check each individual DB for unattached galleries and attach them 1 by 1
			for (true) {
				//execute big function that adds/removes/modifies galleries
				if sellerfoxPkg.Post_ebay(db, dbmap, client_db, client_id, sellerfoxPkg.User_token, log_file) == false {
					fmt.Println("break. no more galleries to attach. ")
					break;
				}
				//check if gallery was added max amount of times
				gallery_counter++;
				//fmt.Print(gallery_counter, " ") //delete later
				if gallery_counter == Auction_count {
					fmt.Println("break. only", Auction_count ,"galleries max from same user at once. ")
					break;
				}
			}
			fmt.Println("actions with client id", client_id, "are finished.\n")
		}
	}
	//log
	if sellerfoxPkg.Debug == true {
		if _, err = log_file.WriteString("Script has finished working.\r\n\r\n\r\n"); err != nil { fmt.Println("error writing to file 6:", err) }
	}
}
