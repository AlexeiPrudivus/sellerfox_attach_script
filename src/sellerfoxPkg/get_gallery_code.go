package sellerfoxPkg

import (
	"fmt"
	"github.com/coopernurse/gorp"
	"strconv"
	"crypto/md5"
	"io"
	"encoding/hex"
	"strings"
)

func Get_gallery_code(gallery_option_border_type string, row []Single_row_gallery_view, h_alignment string, dbmap *gorp.DbMap)(string) {
	/* Replace gallery variables
	# {-width-}
	# {-height-}
	# {-user_id-}
	# {-gallery_id-}
	# {-sf_style_host-}
	*/

	show_categories := "1"
	//$sfg_gallery_row = mysql::get_row("SELECT `".$CONFIG['db'].'_'.add_zeros($row["sf_login_id"],5)."`.show_categories FROM sfg_gallery WHERE id=".$row['gallery_id']);

	query_text := "SELECT show_categories FROM sellerfox_"+Add_zeros(row[0].Sf_login_id)+".sfg_gallery WHERE id="+strconv.Itoa(row[0].Gallery_id)

	//a single row(with data about a single gallery)
	var row_cat []Sfg_gallery
	//SQL query execute and write to variable row
	_, err := dbmap.Select(&row_cat, query_text)
	if err != nil {
		fmt.Println("SQL query error. ", err)
	}
	show_categories = strconv.Itoa(row_cat[0].Show_categories)

	var out_map map[string]string
	out_map = make(map[string]string)

//============================================= Generate different gallery codes ============================================//
	//generating load_gallery_code
	load_gallery_code := "<script type=\"text/javascript\">"
	load_gallery_code += "/*ItemID*/"
	load_gallery_code += "a=new Array(\"<sc\",\"ar SF\",\"cume\",\"cati\",\"/scri\");b=new Array(\"ript>v\",\"L=do\",\"nt.lo\",\"on;<\",\"pt>\");for(i=0;i<5;i++){document.write(a[i]+b[i]);} var l=SFL;var itemid=0;if(!l.search){l=l.pathname;if(l){var s=l.indexOf(\"Z\",\"itemZ\");if(l.length>1&&s>-1){var x=l.substring(s+1,s+20);itemid=x.substring(0,x.indexOf(\"Q\"));}}}else{l=l.search;if(l){var s=l.split(\"&\");for(var i=0;i<=s.length;i++){if(s[i]){if(s[i].substring(0,5)=='item='){var itemid=s[i].substring(5,s[i].length);break;}}}}}"
	load_gallery_code += "/*End of ItemID*/"
	load_gallery_code += "/*Gallery*/"
	load_gallery_code += "document.write('<object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" codebase=\"http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0\" width=\"{-width-}\" height=\"{-height-}\" id=\"sellerfox\" >');"
	load_gallery_code += "document.write('<param name=\"movie\" value=\"http://gallery.sellerfox.de/swf/sf3.swf\" />');"
	load_gallery_code += "document.write('<param name=\"quality\" value=\"high\" />');"
	load_gallery_code += "document.write('<param name=\"scale\" value=\"noscale\" />');"
	load_gallery_code += "document.write('<param name=\"salign\" value=\"lt\" />');"

	if show_categories != "6" {
		load_gallery_code += "document.write('<param name=\"wmode\" value=\"transparent\" />'); "
	}

	load_gallery_code += "document.write('<param name=\"bgcolor\" value=\"#ffffff\" />');"
	load_gallery_code += "document.write('<param name=\"allowScriptAccess\" value=\"always\" />');"
	load_gallery_code += "document.write('<param name=\"FlashVars\" value=\"breite={-width-}&hoehe={-height-}&GNR={-gallery_id-}&VKNR={-user_id-}&LANG="+row[0].Gallery_language+"&Itemid='+itemid+'\" />');"
	load_gallery_code += "document.write('<embed src=\"http://gallery.sellerfox.de/swf/sf3.swf\" quality=\"high\" scale=\"noscale\" salign=\"lt\" "

	if show_categories != "6" {
		load_gallery_code += "wmode=\"transparent\" "
	}

	load_gallery_code += "bgcolor=\"#ffffff\" width=\"{-width-}\" height=\"{-height-}\" name=\"sellerfox\"  FlashVars=\"breite={-width-}&hoehe={-height-}&GNR={-gallery_id-}&VKNR={-user_id-}&LANG="+row[0].Gallery_language+"&Itemid='+itemid+'\" allowScriptAccess = \"always\" type=\"application/x-shockwave-flash\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" />');"
	load_gallery_code += "document.write('</object>');"
	load_gallery_code += "/*End of Gallery*/"
	load_gallery_code += "/*Static*/"
	load_gallery_code += "if(true != sellerfoxstatic)"
	load_gallery_code += "{"
		load_gallery_code += "document.write('<img src=\"http://static.sellerfox.eu/index.php?page=tracking&v={-user_id-}&i='+itemid+'\" height=\"1\" width=\"1\" border=\"0\">');"
		load_gallery_code += "var sellerfoxstatic = true;"
	load_gallery_code += "}"
	load_gallery_code += "/*End of Static*/"
	load_gallery_code += "</script>"

	//writing load_gallery_code to out_map
	out_map["load_gallery_code"] = load_gallery_code

	//generating gallery_code_ns
	load_gallery_code_ns := "<!-- Gallery Code -->"
	load_gallery_code_ns += "<object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" codebase=\"http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0\" width=\"{-width-}\" height=\"{-height-}\" id=\"sellerfox\">"
	load_gallery_code_ns += "<param name=\"movie\" value=\"http://gallery.sellerfox.de/swf/sf3.swf\" />"
	load_gallery_code_ns += "<param name=\"quality\" value=\"high\" />"
	load_gallery_code_ns += "<param name=\"scale\" value=\"noscale\" />"
	load_gallery_code_ns += "<param name=\"salign\" value=\"lt\" />"

	if show_categories != "6" {
		load_gallery_code_ns += "<param name=\"wmode\" value=\"transparent\" />"
	}

	load_gallery_code_ns += "<param name=\"allowScriptAccess\" value=\"always\" />"
	load_gallery_code_ns += "<param name=\"bgcolor\" value=\"#ffffff\" />"
	load_gallery_code_ns += "<param name=\"FlashVars\" value=\"breite={-width-}&hoehe={-height-}&GNR={-gallery_id-}&VKNR={-user_id-}&LANG="+row[0].Gallery_language+"\" />"
	load_gallery_code_ns += "<embed src=\"http://gallery.sellerfox.de/swf/sf3.swf\" quality=\"high\" scale=\"noscale\" salign=\"lt\" "

	if show_categories != "6" {
		load_gallery_code_ns += "wmode=\"transparent\" "
	}

	load_gallery_code_ns += "bgcolor=\"#ffffff\" width=\"{-width-}\" height=\"{-height-}\" name=\"sellerfox\"  FlashVars=\"breite={-width-}&hoehe={-height-}&GNR={-gallery_id-}&VKNR={-user_id-}&LANG="+row[0].Gallery_language+"\" allowScriptAccess = \"always\" type=\"application/x-shockwave-flash\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" />"
	load_gallery_code_ns += "</object>"

	//writing load_gallery_code_ns to out_map
	out_map["load_gallery_code_ns"] = load_gallery_code_ns

	//generating load_gallery_code_uni
	load_gallery_code_uni := "/*ItemID*/"
	load_gallery_code_uni += "a=new Array(\"<sc\",\"ar SF\",\"cume\",\"cati\",\"/scri\");b=new Array(\"ript>v\",\"L=do\",\"nt.lo\",\"on;<\",\"pt>\");for(i=0;i<5;i++){document.write(a[i]+b[i]);} var l=SFL;var itemid=0;if(!l.search){l=l.pathname;if(l){var s=l.indexOf(\"Z\",\"itemZ\");if(l.length>1&&s>-1){var x=l.substring(s+1,s+20);itemid=x.substring(0,x.indexOf(\"Q\"));}}}else{l=l.search;if(l){var s=l.split(\"&\");for(var i=0;i<=s.length;i++){if(s[i]){if(s[i].substring(0,5)==\"item=\"){var itemid=s[i].substring(5,s[i].length);break;}}}}}"
	load_gallery_code_uni += "/*End of ItemID*/"
	load_gallery_code_uni += "/*Gallery*/"
	load_gallery_code_uni += "document.write('<object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" codebase=\"http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0\" width=\"{-width-}\" height=\"{-height-}\" id=\"sellerfox\" >');"
	load_gallery_code_uni += "document.write('<param name=\"movie\" value=\"http://gallery.sellerfox.de/swf/sf3.swf\" />');"
	load_gallery_code_uni += "document.write('<param name=\"quality\" value=\"high\" />');"
	load_gallery_code_uni += "document.write('<param name=\"scale\" value=\"noscale\" />');"
	load_gallery_code_uni += "document.write('<param name=\"salign\" value=\"lt\" />');"

	if show_categories != "6" {
		load_gallery_code_uni += "document.write('<param name=\"wmode\" value=\"transparent\" />');"
	}

	load_gallery_code_uni += "	document.write('<param name=\"allowScriptAccess\" value=\"always\" />');"
	load_gallery_code_uni += "document.write('<param name=\"bgcolor\" value=\"#ffffff\" />');"
	load_gallery_code_uni += "document.write('<param name=\"FlashVars\" value=\"breite={-width-}&hoehe={-height-}&GNR='+gallery_id+'&VKNR={-user_id-}&LANG="+row[0].Gallery_language+"&Itemid='+itemid+'\" />');"
	load_gallery_code_uni += "document.write('<embed src=\"http://gallery.sellerfox.de/swf/sf3.swf\" quality=\"high\" scale=\"noscale\" salign=\"lt\" "

	if show_categories != "6" {
		load_gallery_code_uni += "wmode=\"transparent\" "
	}

	load_gallery_code_uni += "bgcolor=\"#ffffff\" width=\"{-width-}\" height=\"{-height-}\" name=\"sellerfox\" FlashVars=\"breite={-width-}&hoehe={-height-}&GNR='+gallery_id+'&VKNR={-user_id-}&LANG="+row[0].Gallery_language+"&Itemid='+itemid+'\" allowScriptAccess = \"always\" type=\"application/x-shockwave-flash\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" />');"
	load_gallery_code_uni += "document.write('</object>');"
	load_gallery_code_uni += "/*End of Gallery*/"
	load_gallery_code_uni += "/*Static*/"
	load_gallery_code_uni += "if(true != sellerfoxstatic)"
	load_gallery_code_uni += "{"
		load_gallery_code_uni += "document.write('<img src=\"http://static.sellerfox.eu/index.php?page=tracking&v={-user_id-}&i='+itemid+'\" height=\"1\" width=\"1\" border=\"0\">');"
		load_gallery_code_uni += "var sellerfoxstatic = true;"
	load_gallery_code_uni += "}"
	load_gallery_code_uni += "/*End of Static*/"

	//writing load_gallery_code_uni to out_map
	out_map["load_gallery_code_uni"] = load_gallery_code_uni

	//generating load_gallery_preview_code
	load_gallery_preview_code := "<script type=\"text/javascript\">"
	load_gallery_preview_code += "/*Die Gallerie*/"
	load_gallery_preview_code += "document.write('<object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" codebase=\"http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0\" width=\"{-width-}\" height=\"{-height-}\" id=\"sellerfox\" >');"
	load_gallery_preview_code += "document.write('<param name=\"movie\" value=\"http://gallery.sellerfox.de/swf/sf3.swf\" />');"
	load_gallery_preview_code += "document.write('<param name=\"quality\" value=\"high\" />');"
	load_gallery_preview_code += "document.write('<param name=\"scale\" value=\"noscale\" />');"
	load_gallery_preview_code += "document.write('<param name=\"salign\" value=\"lt\" />');"

	if show_categories != "6" {
		load_gallery_preview_code += "document.write('<param name=\"wmode\" value=\"transparent\" />');"
	}

	load_gallery_preview_code += "document.write('<param name=\"allowScriptAccess\" value=\"always\" />');"
	load_gallery_preview_code += "document.write('<param name=\"bgcolor\" value=\"#ffffff\" />');"
	load_gallery_preview_code += "document.write('<param name=\"FlashVars\" value=\"breite={-width-}&hoehe={-height-}&GNR={-gallery_id-}&VKNR={-user_id-}&LANG="+row[0].Gallery_language+"&Itemid=0\" />');"
	load_gallery_preview_code += "document.write('<embed src=\"http://gallery.sellerfox.de/swf/sf3.swf\" quality=\"high\" scale=\"noscale\" salign=\"lt\" "

	if show_categories != "6" {
		load_gallery_preview_code += "wmode=\"transparent\" "
	}

	load_gallery_preview_code += "bgcolor=\"#ffffff\" width=\"{-width-}\" height=\"{-height-}\" name=\"sellerfox\" FlashVars=\"breite={-width-}&hoehe={-height-}&GNR={-gallery_id-}&VKNR={-user_id-}&LANG="+row[0].Gallery_language+"&Itemid=0\" allowScriptAccess = \"always\" type=\"application/x-shockwave-flash\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" />');"
	load_gallery_preview_code += "document.write('</object>');"
	load_gallery_preview_code += "/*Ende die Gallerie*/"
	load_gallery_preview_code += "</script>"

	//writing load_gallery_preview_code to out_map
	out_map["load_gallery_preview_code"] = load_gallery_preview_code

	load_gallery_static_code := "<!-- Begin of Sellerfox Static Code -->"
	load_gallery_static_code += "<script type=\"text/javascript\">"
	load_gallery_static_code += "if(false == sellerfoxstatic)"
	load_gallery_static_code += "{"
		load_gallery_static_code += "var l=top.location;var itemid=0;if(!l.search){l=l.pathname;if(l){var s=l.indexOf(\"Z\",\"itemZ\");if(l.length>1&&s>-1){var x=l.substring(s+1,s+20);itemid=x.substring(0,x.indexOf(\"Q\"));}}}else{l=l.search;if(l){var s=l.split(\"&\");for(var i=0;i<=s.length;i++){if(s[i]){if(s[i].substring(0,5)==\"item=\"){var itemid=s[i].substring(5,s[i].length);break;}}}}}"
		load_gallery_static_code += "document.write('<img src=\"http://static.sellerfox.eu/index.php?page=tracking&v={-user_id-}&i='+itemid+'\" height=\"1\" width=\"1\" border=\"0\">'); var sellerfoxstatic = true;"
	load_gallery_static_code += "}"
	load_gallery_static_code += "</script>"
	load_gallery_static_code += "<noscript>"
	load_gallery_static_code += "<img src=\"http://static.sellerfox.eu/index.php?page=tracking&v={-user_id-}\" height=\"1\" width=\"1\" border=\"0\">"
	load_gallery_static_code += "</noscript>"
	load_gallery_static_code += "<!-- End of Sellerfox Static Code -->"

	//writing load_gallery_static_code to out_map
	out_map["load_gallery_static_code"] = load_gallery_static_code

	//generating load_gallery_admin_preview_code
	load_gallery_admin_preview_code := "<script type=\"text/javascript\">"
	load_gallery_admin_preview_code += "/*Die Gallerie*/"
	load_gallery_admin_preview_code += "document.write('<object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" codebase=\"https://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0\" width=\"{-width-}\" height=\"{-height-}\" id=\"sellerfox\" >');"
	load_gallery_admin_preview_code += "document.write('<param name=\"movie\" value=\"https://www.sellerfox.eu/swf/sf3.swf\" />');"
	load_gallery_admin_preview_code += "document.write('<param name=\"quality\" value=\"high\" />');"
	load_gallery_admin_preview_code += "document.write('<param name=\"scale\" value=\"noscale\" />');"
	load_gallery_admin_preview_code += "document.write('<param name=\"salign\" value=\"lt\" />');"

	if show_categories != "6" {
		load_gallery_admin_preview_code += "document.write('<param name=\"wmode\" value=\"transparent\" />');"
	}

	load_gallery_admin_preview_code += "document.write('<param name=\"allowScriptAccess\" value=\"always\" />');"
	load_gallery_admin_preview_code += "document.write('<param name=\"bgcolor\" value=\"#ffffff\" />');"
	load_gallery_admin_preview_code += "document.write('<param name=\"FlashVars\" value=\"breite={-width-}&hoehe={-height-}&GNR={-gallery_id-}&VKNR={-user_id-}&LANG="+row[0].Gallery_language+"&Itemid=0\" />');"
	load_gallery_admin_preview_code += "document.write('<embed src=\"https://www.sellerfox.eu/swf/sf3.swf\" quality=\"high\" scale=\"noscale\" salign=\"lt\" "

	if show_categories != "6" {
		load_gallery_admin_preview_code += "wmode=\"transparent\" "
	}

	load_gallery_admin_preview_code += "bgcolor=\"#ffffff\" width=\"{-width-}\" height=\"{-height-}\" name=\"sellerfox\"  FlashVars=\"breite={-width-}&hoehe={-height-}&GNR={-gallery_id-}&VKNR={-user_id-}&LANG="+row[0].Gallery_language+"&Itemid=0\" allowScriptAccess = \"always\" type=\"application/x-shockwave-flash\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" />');"
	load_gallery_admin_preview_code += "document.write('</object>');"
	load_gallery_admin_preview_code += "/*Ende die Gallerie*/"
	load_gallery_admin_preview_code += "</script>"

	//writing load_gallery_static_code to out_map
	out_map["load_gallery_admin_preview_code"] = load_gallery_admin_preview_code
	//fmt.Println(out_map) //for testing. delete later
//======================================================= Load borders ======================================================//
	//load_border_type := []string{"Kein Rahmen", "Helblau mit Schalgschatten", "Grau", "Schwarz", "Grau 2", "Braun", "Graue Linie", "Rot mit schein", "Orange", "Grau 3", "Grau 4", "Hellgrau Schlagschatten"}
	//fmt.Println(load_border_type) //delete later

	var load_border_type_borders [12][2]string
	//Kein Rahmen
	load_border_type_borders[0][0] = ""
	load_border_type_borders[0][1] = ""
	//Helblau mit Schalgschatten
	load_border_type_borders[1][0] = "<table width='{-width-}' border='0' cellspacing='0' cellpadding='0'>" +
			"<tr>" +
			"<td><img src='{-sf_style_host-}002/re_1.gif?v={-user_id-}' width='10' height='10'></td>" +
			"<td background='{-sf_style_host-}002/rb_1.gif?v={-user_id-}'><img src='{-sf_style_host-}002/rb_1.gif?v={-user_id-}' width='4' height='10'></td>" +
			"<td><img src='{-sf_style_host-}002/re_2.gif?v={-user_id-}' width='10' height='10'></td>" +
			"</tr>" +
			"<tr>" +
			"<td background='{-sf_style_host-}002/rb_4.gif?v={-user_id-}'><img src='{-sf_style_host-}002/rb_4.gif?v={-user_id-}' width='10' height='6'></td>" +
			"<td width='100%' height='100' bgcolor='#FFFFFF'>"
	load_border_type_borders[1][1] = "</td>" +
			"<td background='{-sf_style_host-}002/rb_2.gif?v={-user_id-}'><img src='{-sf_style_host-}002/rb_2.gif?v={-user_id-}' width='10' height='3'></td>" +
			"</tr>" +
			"<tr>" +
			"<td><img src='{-sf_style_host-}002/re_4.gif?v={-user_id-}' width='10' height='10'></td>" +
			"<td background='{-sf_style_host-}002/rb_3.gif?v={-user_id-}'><img src='{-sf_style_host-}002/rb_3.gif?v={-user_id-}' width='5' height='10'></td>" +
			"<td><img src='{-sf_style_host-}002/re_3.gif?v={-user_id-}' width='10' height='10'></td>" +
			"</tr>" +
			"</table>"
	//Grau
	load_border_type_borders[2][0] = "<table width='{-width-}' border='0' cellspacing='0' cellpadding='0' >" +
			"<tr>" +
			"<td><img src='{-sf_style_host-}003/c1.gif?v={-user_id-}' width='18' height='33'></td>" +
			"<td background='{-sf_style_host-}003/b1.gif?v={-user_id-}'><img src='{-sf_style_host-}003/b1.gif?v={-user_id-}' width='5' height='33'></td>" +
			"<td><img src='{-sf_style_host-}003/c2.gif?v={-user_id-}' width='18' height='33'></td>" +
			"</tr>" +
			"<tr>" +
			"<td background='{-sf_style_host-}003/b4.gif?v={-user_id-}'><img src='{-sf_style_host-}003/b4.gif?v={-user_id-}' width='18' height='4'></td>" +
			"<td width='100%' height='100%' bgcolor='#FFFFFF'>"
	load_border_type_borders[2][1] = "</td>" +
			"<td background='{-sf_style_host-}003/b2.gif?v={-user_id-}'><img src='{-sf_style_host-}003/b2.gif?v={-user_id-}' width='18' height='4'></td>" +
			"</tr>" +
			"<tr>" +
			"<td><img src='{-sf_style_host-}003/c4.gif?v={-user_id-}' width='18' height='31'></td>" +
			"<td background='{-sf_style_host-}003/b3.gif?v={-user_id-}'><img src='{-sf_style_host-}003/b3.gif?v={-user_id-}' width='4' height='31'></td>" +
			"<td><img src='{-sf_style_host-}003/c3.gif?v={-user_id-}' width='18' height='31'></td>" +
			"</tr>" +
			"</table>"
	//Schwarz
	load_border_type_borders[3][0] = "<table width='{-width-}' border='0' cellspacing='0' cellpadding='0' >" +
			"<tr>" +
			"<td><img src='{-sf_style_host-}004/c1.gif?v={-user_id-}' width='8' height='8'></td>" +
			"<td background='{-sf_style_host-}004/b1.gif?v={-user_id-}'><img src='{-sf_style_host-}004/b1.gif?v={-user_id-}' width='8' height='8'></td>" +
			"<td><img src='{-sf_style_host-}004/c2.gif?v={-user_id-}' width='8' height='8'></td>" +
			"</tr>" +
			"<tr>" +
			"<td background='{-sf_style_host-}004/b4.gif?v={-user_id-}'><img src='{-sf_style_host-}004/b4.gif?v={-user_id-}' width='8' height='8'></td>" +
			"<td width='100%' height='100%' bgcolor='#111111'>"
	load_border_type_borders[3][1] = "</td>" +
			"<td background='{-sf_style_host-}004/b2.gif?v={-user_id-}'><img src='{-sf_style_host-}004/b2.gif?v={-user_id-}' width='8' height='8'></td>" +
			"</tr>" +
			"<tr>" +
			"<td><img src='{-sf_style_host-}004/c4.gif?v={-user_id-}' width='8' height='8'></td>" +
			"<td background='{-sf_style_host-}004/b3.gif?v={-user_id-}'><img src='{-sf_style_host-}004/b3.gif?v={-user_id-}' width='8' height='8'></td>" +
			"<td><img src='{-sf_style_host-}004/c3.gif?v={-user_id-}' width='8' height='8'></td>" +
			"</tr>" +
			"</table>"
	//Grau 2
	load_border_type_borders[4][0] = "<table width='{-width-}' border='0' cellspacing='0' cellpadding='0' >" +
			"<tr>" +
			"<td><img src='{-sf_style_host-}008/c1.gif?v={-user_id-}' ></td>" +
			"<td background='{-sf_style_host-}008/b1.gif?v={-user_id-}'><img src='{-sf_style_host-}008/b1.gif?v={-user_id-}' ></td>" +
			"<td><img src='{-sf_style_host-}008/c2.gif?v={-user_id-}' ></td>" +
			"</tr>" +
			"<tr>" +
			"<td background='{-sf_style_host-}008/b4.gif?v={-user_id-}'><img src='{-sf_style_host-}008/b4.gif?v={-user_id-}' ></td>" +
			"<td width='100%' height='100%' bgcolor='#515763'>"
	load_border_type_borders[4][1] = "</td>" +
			"<td background='{-sf_style_host-}008/b2.gif?v={-user_id-}'><img src='{-sf_style_host-}008/b2.gif?v={-user_id-}' ></td>" +
			"</tr>" +
			"<tr>" +
			"<td><img src='{-sf_style_host-}008/c4.gif?v={-user_id-}' ></td>" +
			"<td background='{-sf_style_host-}008/b3.gif?v={-user_id-}'><img src='{-sf_style_host-}008/b3.gif?v={-user_id-}' ></td>" +
			"<td><img src='{-sf_style_host-}008/c3.gif?v={-user_id-}'></td>" +
			"</tr>" +
			"</table>"
	//Braun
	load_border_type_borders[5][0] = "<table width='{-width-}' border='0' cellspacing='0' cellpadding='0' >" +
			"<tr>" +
			"<td><img src='{-sf_style_host-}010/c1.gif?v={-user_id-}' ></td>" +
			"<td background='{-sf_style_host-}010/b1.gif?v={-user_id-}'><img src='{-sf_style_host-}010/b1.gif?v={-user_id-}' ></td>" +
			"<td><img src='{-sf_style_host-}010/c2.gif?v={-user_id-}' ></td>" +
			"</tr>" +
			"<tr>" +
			"<td background='{-sf_style_host-}010/b4.gif?v={-user_id-}'><img src='{-sf_style_host-}010/b4.gif?v={-user_id-}' ></td>" +
			"<td width='100%' height='100%' bgcolor='#A8571C'>"
	load_border_type_borders[5][1] = "</td>" +
			"<td background='{-sf_style_host-}010/b2.gif?v={-user_id-}'><img src='{-sf_style_host-}010/b2.gif?v={-user_id-}' ></td>" +
			"</tr>" +
			"<tr>" +
			"<td><img src='{-sf_style_host-}010/c4.gif?v={-user_id-}' ></td>" +
			"<td background='{-sf_style_host-}010/b3.gif?v={-user_id-}'><img src='{-sf_style_host-}010/b3.gif?v={-user_id-}' ></td>" +
			"<td><img src='{-sf_style_host-}010/c3.gif?v={-user_id-}'></td>" +
			"</tr>" +
			"</table>"
	//Graue Linie
	load_border_type_borders[6][0] = "<table width='{-width-}' border='0' cellspacing='0' cellpadding='0' >" +
			"<tr>" +
			"<td><img src='{-sf_style_host-}000/g1x1.gif?v={-user_id-}' ></td>" +
			"<td background='{-sf_style_host-}000/g1x1.gif?v={-user_id-}'><img src='{-sf_style_host-}000/g1x1.gif?v={-user_id-}' ></td>" +
			"<td><img src='{-sf_style_host-}000/g1x1.gif?v={-user_id-}' ></td>" +
			"</tr>" +
			"<tr>" +
			"<td background='{-sf_style_host-}000/g1x1.gif?v={-user_id-}'><img src='{-sf_style_host-}000/g1x1.gif?v={-user_id-}' ></td>" +
			"<td width='100%' height='100%' bgcolor='#FFFFFF'>"
	load_border_type_borders[6][1] = "</td>" +
			"<td background='{-sf_style_host-}000/g1x1.gif?v={-user_id-}'><img src='{-sf_style_host-}000/g1x1.gif?v={-user_id-}' ></td>" +
			"</tr>" +
			"<tr>" +
			"<td><img src='{-sf_style_host-}000/g1x1.gif?v={-user_id-}' ></td>" +
			"<td background='{-sf_style_host-}000/g1x1.gif?v={-user_id-}'><img src='{-sf_style_host-}000/g1x1.gif?v={-user_id-}' ></td>" +
			"<td><img src='{-sf_style_host-}000/g1x1.gif?v={-user_id-}'></td>" +
			"</tr>" +
			"</table>"
	//Rot mit schein
	load_border_type_borders[7][0] = "<table width='{-width-}' border='0' cellspacing='0' cellpadding='0' >" +
			"<tr>" +
			"<td><img src='{-sf_style_host-}011/c1.gif?v={-user_id-}' ></td>" +
			"<td background='{-sf_style_host-}011/b1.gif?v={-user_id-}'><img src='{-sf_style_host-}011/b1.gif?v={-user_id-}' ></td>" +
			"<td><img src='{-sf_style_host-}011/c2.gif?v={-user_id-}' ></td>" +
			"</tr>" +
			"<tr>" +
			"<td background='{-sf_style_host-}011/b4.gif?v={-user_id-}'><img src='{-sf_style_host-}011/b4.gif?v={-user_id-}' ></td>" +
			"<td width='100%' height='100%' bgcolor='#A80000'>"
	load_border_type_borders[7][1] = "</td>" +
			"<td background='{-sf_style_host-}011/b2.gif?v={-user_id-}'><img src='{-sf_style_host-}011/b2.gif?v={-user_id-}' ></td>" +
			"</tr>" +
			"<tr>" +
			"<td><img src='{-sf_style_host-}011/c4.gif?v={-user_id-}' ></td>" +
			"<td background='{-sf_style_host-}011/b3.gif?v={-user_id-}'><img src='{-sf_style_host-}011/b3.gif?v={-user_id-}' ></td>" +
			"<td><img src='{-sf_style_host-}011/c3.gif?v={-user_id-}'></td>" +
			"</tr>" +
			"</table>"
	//Orange
	load_border_type_borders[8][0] = "<table width='{-width-}' border='0' cellspacing='0' cellpadding='0' >" +
			"<tr>" +
			"<td><img src='{-sf_style_host-}012/c1.gif?v={-user_id-}' ></td>" +
			"<td background='{-sf_style_host-}012/b1.gif?v={-user_id-}'><img src='{-sf_style_host-}012/b1.gif?v={-user_id-}' ></td>" +
			"<td><img src='{-sf_style_host-}012/c2.gif?v={-user_id-}' ></td>" +
			"</tr>" +
			"<tr>" +
			"<td background='{-sf_style_host-}012/b4.gif?v={-user_id-}'><img src='{-sf_style_host-}012/b4.gif?v={-user_id-}' ></td>" +
			"<td width='100%' height='100%' bgcolor='#FF8702'>"
	load_border_type_borders[8][1] = "</td>" +
			"<td background='{-sf_style_host-}012/b2.gif?v={-user_id-}'><img src='{-sf_style_host-}012/b2.gif?v={-user_id-}' ></td>" +
			"</tr>" +
			"<tr>" +
			"<td><img src='{-sf_style_host-}012/c4.gif?v={-user_id-}' ></td>" +
			"<td background='{-sf_style_host-}012/b3.gif?v={-user_id-}'><img src='{-sf_style_host-}012/b3.gif?v={-user_id-}' ></td>" +
			"<td><img src='{-sf_style_host-}012/c3.gif?v={-user_id-}'></td>" +
			"</tr>" +
			"</table>"
	//Grau 3
	load_border_type_borders[9][0] = "<table width='{-width-}' border='0' cellspacing='0' cellpadding='0'>" +
			"<tr>" +
			"<td><img src='{-sf_style_host-}013/c1.gif?v={-user_id-}' ></td>" +
			"<td background='{-sf_style_host-}013/b1.gif?v={-user_id-}'><img src='{-sf_style_host-}013/b1.gif?v={-user_id-}' ></td>" +
			"<td><img src='{-sf_style_host-}013/c2.gif?v={-user_id-}' ></td>" +
			"</tr>" +
			"<tr>" +
			"<td background='{-sf_style_host-}013/b4.gif?v={-user_id-}'><img src='{-sf_style_host-}013/b4.gif?v={-user_id-}' ></td>" +
			"<td width='100%' height='100%' bgcolor='#1F242D'>"
	load_border_type_borders[9][1] = "</td>" +
			"<td background='{-sf_style_host-}013/b2.gif?v={-user_id-}'><img src='{-sf_style_host-}013/b2.gif?v={-user_id-}' ></td>" +
			"</tr>" +
			"<tr>" +
			"<td><img src='{-sf_style_host-}013/c4.gif?v={-user_id-}' ></td>" +
			"<td background='{-sf_style_host-}013/b3.gif?v={-user_id-}'><img src='{-sf_style_host-}013/b3.gif?v={-user_id-}' ></td>" +
			"<td><img src='{-sf_style_host-}013/c3.gif?v={-user_id-}'></td>" +
			"</tr>" +
			"</table>"
	//Grau 4
	load_border_type_borders[10][0] = "<table width='{-width-}' border='0' cellspacing='0' cellpadding='0' >" +
			"<tr>" +
			"<td><img src='{-sf_style_host-}014/c1.gif?v={-user_id-}' ></td>" +
			"<td background='{-sf_style_host-}014/b1.gif?v={-user_id-}'><img src='{-sf_style_host-}014/b1.gif?v={-user_id-}' ></td>" +
			"<td><img src='{-sf_style_host-}014/c2.gif?v={-user_id-}' ></td>" +
			"</tr>" +
			"<tr>" +
			"<td background='{-sf_style_host-}014/b4.gif?v={-user_id-}'><img src='{-sf_style_host-}014/b4.gif?v={-user_id-}' ></td>" +
			"<td width='100%' height='100%' bgcolor='#FFFFFF'>"
	load_border_type_borders[10][1] = "</td>" +
			"<td background='{-sf_style_host-}014/b2.gif?v={-user_id-}'><img src='{-sf_style_host-}014/b2.gif?v={-user_id-}' ></td>" +
			"</tr>" +
			"<tr>" +
			"<td><img src='{-sf_style_host-}014/c4.gif?v={-user_id-}' ></td>" +
			"<td background='{-sf_style_host-}014/b3.gif?v={-user_id-}'><img src='{-sf_style_host-}014/b3.gif?v={-user_id-}' ></td>" +
			"<td><img src='{-sf_style_host-}014/c3.gif?v={-user_id-}'></td>" +
			"</tr>" +
			"</table>"
	//Hellgrau Schlagschatten
	load_border_type_borders[11][0] = "<table width='{-width-}' border='0' cellspacing='0' cellpadding='0'>" +
			"<tr>" +
			"<td><img src='{-sf_style_host-}015/c1.gif?v={-user_id-}' ></td>" +
			"<td background='{-sf_style_host-}015/b1.gif?v={-user_id-}'><img src='{-sf_style_host-}015/b1.gif?v={-user_id-}' ></td>" +
			"<td><img src='{-sf_style_host-}015/c2.gif?v={-user_id-}' ></td>" +
			"</tr>" +
			"<tr>" +
			"<td background='{-sf_style_host-}015/b4.gif?v={-user_id-}'><img src='{-sf_style_host-}015/b4.gif?v={-user_id-}' ></td>" +
			"<td width='100%' height='100%' bgcolor='#FFFFFF'>"
	load_border_type_borders[11][1] = "</td>" +
			"<td background='{-sf_style_host-}015/b2.gif?v={-user_id-}'><img src='{-sf_style_host-}015/b2.gif?v={-user_id-}' ></td>" +
			"</tr>" +
			"<tr>" +
			"<td><img src='{-sf_style_host-}015/c4.gif?v={-user_id-}' ></td>" +
			"<td background='{-sf_style_host-}015/b3.gif?v={-user_id-}'><img src='{-sf_style_host-}015/b3.gif?v={-user_id-}' ></td>" +
			"<td><img src='{-sf_style_host-}015/c3.gif?v={-user_id-}'></td>" +
			"</tr>" +
			"</table>"
//================================ Generate gallery_code depending on border options from DB ================================//
	gallery_code := ""
	if gallery_option_border_type != "" {
		//if a border style option exists in DB
		gallery_option_border_type_int, _ := strconv.Atoi(gallery_option_border_type)
		border_0 := load_border_type_borders[gallery_option_border_type_int][0]
		border_1 := load_border_type_borders[gallery_option_border_type_int][1]
		gallery_code = border_0+load_gallery_code+border_1
	} else {
		//if border style option does not exist in DB/is empty
		gallery_code = load_gallery_code
	}

	md5_hash := md5.New()
	io.WriteString(md5_hash, strconv.Itoa(row[0].Sf_login_id)+strconv.Itoa(row[0].Gallery_id))
	gallery_code = "<div id=\"sfg_"+hex.EncodeToString(md5_hash.Sum(nil))+"\" style='width: 100%; text-align: "+h_alignment+";'>"+gallery_code+"</div>"
	// in th eoriginal script is used utf8_encode($galleryName)
	gallery_code = "<!-- Begin of Sellerfox Gallery \""+row[0].Gallery_name+"\" --><br />"+gallery_code+"<br /><!-- End of Sellerfox Gallery \""+row[0].Gallery_name+"\" -->"
	//fmt.Println(gallery_code) //delete later

	//replace placeholders with values
	gallery_code = strings.Replace(gallery_code, "{-width-}", strconv.Itoa(row[0].Gallery_width), -1)
	gallery_code = strings.Replace(gallery_code, "{-height-}", strconv.Itoa(row[0].Gallery_height), -1)
	gallery_code = strings.Replace(gallery_code, "{-user_id-}", strconv.Itoa(row[0].Sf_login_id), -1)
	gallery_code = strings.Replace(gallery_code, "{-gallery_id-}", strconv.Itoa(row[0].Gallery_id), -1)
	if strings.Contains(gallery_code, "https://www.sellerfox.eu") != false {
		gallery_code = strings.Replace(gallery_code, "{-sf_style_host-}", "https://www.sellerfox.eu/swf/style/", -1)
	} else {
		gallery_code = strings.Replace(gallery_code, "{-sf_style_host-}", "http://border.sellerfox.de/swf/style/", -1)
	}

	return gallery_code
}
