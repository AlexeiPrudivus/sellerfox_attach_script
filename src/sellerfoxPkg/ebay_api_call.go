package sellerfoxPkg
import (
	"github.com/PuerkitoBio/goquery"
	_ "github.com/go-sql-driver/mysql"
//	"database/sql"
//	"github.com/coopernurse/gorp"
//	"strconv"
//	"github.com/yvasiyarov/php_session_decoder"
//	"github.com/yvasiyarov/php_session_decoder/php_serialize"
//	"time"
//	"log"
//	"io/ioutil"
//	"reflect"
	"net/http"
	"strings"
	"fmt"
)

func Ebay_api_call(verb, request_xml_body string)(map[string]string){
//=========================================== Generate and execute XmlHttpRequest ===========================================//
	client := &http.Client{}

	//will be used to return result
	var ebay_api_call_return map[string]string
	ebay_api_call_return = make(map[string]string)

	//prudivusPkg.Server_url+strconv.Itoa(item_id) //other url. to recieve html of the item page
	request, err := http.NewRequest("POST", Server_url, strings.NewReader(request_xml_body))
	if err != nil {
		fmt.Println("HttpRequest failed %s", err) //delete later
		ebay_api_call_return["status"] = "0"
		ebay_api_call_return["error"] = "Error generating request"
		ebay_api_call_return["error"] = "-2"
		return ebay_api_call_return
	}

	//add headers to request
	request.Header.Add("X-EBAY-API-COMPATIBILITY-LEVEL", Compatibility_level)
	request.Header.Add("X-EBAY-API-DEV-NAME", Dev_id)
	request.Header.Add("X-EBAY-API-APP-NAME", App_id)
	request.Header.Add("X-EBAY-API-CERT-NAME", Cert_id)
	request.Header.Add("X-EBAY-API-CALL-NAME", verb)
	request.Header.Add("X-EBAY-API-SITEID", Site_id)

	//execute request and get response
	response, err := client.Do(request)
	if err != nil {
		fmt.Printf("%s", err) //delete later
		ebay_api_call_return["status"] = "0"
		ebay_api_call_return["error"] = "Error sending request"
		ebay_api_call_return["error_id"] = "-1"
		return ebay_api_call_return
	}
	defer response.Body.Close()

	//html code of GetItem result
	responseDoc, err := goquery.NewDocumentFromResponse(response)
	if err != nil {
		fmt.Println("%s",err) //delete later
		ebay_api_call_return["status"] = "0"
		ebay_api_call_return["error"] = "Error using goquery"
		ebay_api_call_return["error"] = "-3"
		return ebay_api_call_return
	}
//================================================== if XML file has errors =================================================//
	errors :=responseDoc.Find("Errors") //fmt.Println(errors.Html())
	errors_html, _ := errors.Html()
	if errors_html != "" {
		errors_html = strings.Replace(errors_html, "&#34;", "\"", -1) //"
		errors_html = strings.Replace(errors_html, "&lt;", "<", -1) // <
		errors_html = strings.Replace(errors_html, "&gt;", ">", -1) // >
		errors_html = strings.Replace(errors_html, "&#39;", "'", -1) // '
		errors_html = strings.Replace(errors_html, "&amp;", "&", -1) // &
		//fmt.Println(request_xml_body) //delete later
		//fmt.Println(errors_html) //delete later

		//ebay_api_call_return must have errors or warnings if they will occur
		//set defaults to resultArray
		ebay_api_call_return["status"] = "1"
		ebay_api_call_return["error"] = ""
		ebay_api_call_return["error_id"] = ""
		ebay_api_call_return["warning_status"] = "0"
		ebay_api_call_return["warning"] = ""
		ebay_api_call_return["warning_id"] = ""

		//foreach error in errors
		responseDoc.Find("Errors").Each(func(i int, error *goquery.Selection) {
			//fmt.Println("error number", i) //index //delete later
			if i == 0 {
				//variables used within foreach loop
				short_msg := "";
				long_msg := "";
				error_id := "";

				//delete later
				//error_text := error.Find("longmessage").Text()
				//fmt.Println(error_text)
				error_code := error.Find("SeverityCode").Text()
				//fmt.Println("severity code value:", error_code)

				if error_code == "Warning" {
//============================================= if XML file has warnings/errors =============================================//
					res_warning := ""
					/*if ebay_api_call_return["warning_status"] == "1" {
					continue
					}*/
					ebay_api_call_return["warning_status"] = "1"

					error_id = error.Find("ErrorCode").Text()
					short_msg = error.Find("ShortMessage").Text()
					long_msg = error.Find("LongMessage").Text()

					//fmt.Println("error_id:", error_id) //delete later
					//fmt.Println("short_msg:", short_msg) //delete later
					//fmt.Println("long_msg:", long_msg) //delete later

					res_warning = error_id+":"+short_msg

					//if there is a long message (ie ErrorLevel=1), display it
					if len(long_msg) > 0 {
						res_warning = error_id+":"+long_msg
					}

					ebay_api_call_return["warning_id"] = error_id
					ebay_api_call_return["warning"] = res_warning
				} else if error_code == "Error" {
//================================================== if XML file has errors =================================================//
					res_warning := ""
					/*if ebay_api_call_return["status"] == "0" {
					continue
					}*/
					ebay_api_call_return["status"] = "0"

					error_id = error.Find("ErrorCode").Text()
					short_msg = error.Find("ShortMessage").Text()
					long_msg = error.Find("LongMessage").Text()

					//fmt.Println("error_id:", error_id) //delete later
					//fmt.Println("short_msg:", short_msg) //delete later
					//fmt.Println("long_msg:", long_msg) //delete later

					res_warning = error_id+":"+short_msg

					//if there is a long message (ie ErrorLevel=1), display it
					if len(long_msg) > 0 {
						res_warning = error_id+":"+long_msg
					}

					ebay_api_call_return["error_id"] = error_id
					ebay_api_call_return["error"] = res_warning
				}
			}
		})
		return ebay_api_call_return
	} else {
//================================================= if XML file has no errors ===============================================//
	//get results nodes
	responses := responseDoc.Find(verb+"Response");
	responses_html, _ := responses.Html()
	description, _ := responseDoc.Find("Description").Html()
	//fmt.Println(responses.Html())
	//fmt.Println(responses)
	/*
	* TODO: import proper code that handles elements_to_get being an array or a string
	*/
	/*for _,response := range responses {
		if reflect.TypeOf(elements_to_get).String() == "[]string" { //if elements_to_get is an array(several nodes)

		} else if reflect.TypeOf(elements_to_get).String() == "string" { //if elements_to_get is a string(a single node)
			/* html code
			$elements  = $response->getElementsByTagName($elementsToGet);
			$l = $elements->length;
			for($i=0; $l, $i<$l; $i++) {
			$items[$elementsToGet][]  = $elements->item($i)->nodeValue;
			*
		} else {

		}
	}*/
	//responses := responseDoc.Find(verb+"Response");
	ebay_api_call_return["status"] = "1"
	ebay_api_call_return["items"] = responses_html
	ebay_api_call_return["description"] = description
	return ebay_api_call_return
	}
//description :=responseDoc.Find("Description")
//fmt.Println(description.Html())
}
