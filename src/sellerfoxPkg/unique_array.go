package sellerfoxPkg

//this function returns an array with only unique elements from the original array
func Unique_array(orig_array []string)([]string){

	temp_array := make([]string, len(orig_array))

	k:=0
	for i:=0;i<len(orig_array);i++ {
		u:=0
		for j:=0;j<len(temp_array);j++ {
			if temp_array[j] == orig_array[i] {
				u++
			}
		}
		if u == 0 {
			temp_array[k] = orig_array[i]
			k++
		}
	}

	n:=0
	for i:=0;i<len(temp_array);i++ {
		if temp_array[i] != "" {
			n++
		}
	}

	unique_array := make([]string, n)
	for i:=0;i<len(unique_array);i++ {
		unique_array[i] = temp_array[i]
	}

	return unique_array
}

/*func main() {
	array := []string{"2","2","3","2","3","2","4","2","5","5","5","5","5","5","5","5","7896867"}

	u_array := unique_array(array)
	fmt.Println(u_array)
}*/
