package sellerfoxPkg

import (
	_ "github.com/go-sql-driver/mysql"
	"time"
	"strconv"
	"fmt"
	"database/sql"
	"github.com/yvasiyarov/php_session_decoder/php_serialize"
	"github.com/coopernurse/gorp"
	"strings"
	"os"
)

//big function that adds/removes/modifies galleries
func Post_ebay(db *sql.DB,dbmap *gorp.DbMap, client_db string, client_id int, user_token string, log_file *os.File)(someBool bool) {
	//TODO: write a TITANIC function here
	//var  f, req_sf_login_id, database, userToken, avoid_dublicates, D, siteID string

	//create a map to record script's progress
	var text_debug map[string]string
	text_debug = make(map[string]string)

	t := time.Now().Local()
	text_debug["start_time"] = t.Format("02-01-2006 15:04:05") //script start time
	text_debug["login_id"] = strconv.Itoa(client_id) //customer login id from DB
//============================= execute SQL query to get a single row with data about a gallery =============================//
	//sql query text
	query_text := "SELECT queue.*, settings.ebay_id as Ebay_id  "
	query_text += "FROM " +client_db+".sfg_attach_gallery_queue as queue "
	query_text += "INNER JOIN "+client_db+".sfg_auctions as auction ON queue.item_id = auction.item_id "
	query_text += "INNER JOIN "+client_db+".sfg_settings as settings ON (settings.id=auction.ebay_id AND settings.ebay_token_expire>UNIX_TIMESTAMP()) "
	query_text += "WHERE  queue.process=0 LIMIT 1"

	//a single row(with data about a single gallery)
	var row []Single_row_gallery_view
	//SQL query execute and write to variable row
	_, err := dbmap.Select(&row, query_text)
	if err != nil {
		fmt.Println("SQL query error. ", err)
		return
	}
//================================================== if sql result is empty =================================================//
	if len(row)==0 {
		//update DB to confirm that no more galleries nee dot be attached for this user
		query_text = "Update sellerfox.sf_contract set not_attach_auctions=0 where sf_login_id="+strconv.Itoa(client_id)
		_, err := db.Query(query_text)
		if err != nil {
			fmt.Println("Could not update DB. ", err)
			return
		}

		fmt.Println("No rows in query result. ", row)
		return false
	} else {
//================================================ if sql result is not empty ===============================================//
//============================================= unserialize gallery_option array ============================================//
		//use "github.com/yvasiyarov/php_session_decoder/php_serialize" library to unserialize array gallery_option selected from DB
		decoder := php_serialize.NewUnSerializer(row[0].Gallery_option)
		gOptions_phpValue, err_unserial := decoder.Decode()
		if err_unserial != nil {
			fmt.Println("Could not decode array ", err_unserial)
			return
		}
		gOptions := gOptions_phpValue.(php_serialize.PhpArray)

		//fmt.Println(row[0].Gallery_option) //delete later. test output of the whole serialized gallery_option array
		//fmt.Println(gOptions) //delete later. test output of unserialized gallery_option array
		//fmt.Println(gOptions[php_serialize.PhpValue("design")]) //delete later. test output of "design" field in gallery_option array
		//fmt.Println(gOptions[php_serialize.PhpValue("font_color")]) //delete later. test output of "font_color" field in gallery_option array
//=================================================== working with options ==================================================//
		fmt.Println("working with item_id:",row[0].Item_id)
		/* Available options:
		* 'allow_duplicates'
		* 'realign' // nothing to do, usual flow
		* 'replace' // take old gallery id, check for it in description, remove, then add new one. Then if old gallery not in description remove from it db.
		* 'delete'  // remove from description, remove from db.
		*/
		//write options into text_debug fields
		if gOptions[php_serialize.PhpValue("allow_duplicates")] != nil && gOptions[php_serialize.PhpValue("allow_duplicates")] == 1 {
			Avoid_duplicates = false
			text_debug["avoid_duplicates"] = "false"
		} else if gOptions[php_serialize.PhpValue("allow_duplicates")] == 0 {
			Avoid_duplicates = true
			text_debug["avoid_duplicates"] = "true"
		}

		delete := "false"
		replace := "false"
		if gOptions[php_serialize.PhpValue("delete")] != nil && gOptions[php_serialize.PhpValue("delete")] == 1 {
			delete = "true"
			text_debug["avoid_duplicates"] = "true"
			row[0].Gallery_alignment = "b_center"// gallery_alignment
		} else if gOptions[php_serialize.PhpValue("replace")] != nil && gOptions[php_serialize.PhpValue("replace")] != 0 {
			replace = "true"
		}

		text_debug["delete"] = delete
		text_debug["replace"] = replace
		text_debug["item_id"] = strconv.Itoa(row[0].Item_id) //item_id

		if gOptions[php_serialize.PhpValue("realign")] != nil {
			text_debug["realign"] = strconv.Itoa(gOptions[php_serialize.PhpValue("realign")].(int))
		} else {
			text_debug["realign"] = "0"
		}

		//fmt.Println("avoid_duplicates", text_debug["avoid_duplicates"]) //delete later
//=============================================== set time of processing in DB ==============================================//
		t2 := time.Now().Local()
		unix_time := t2.Unix()//unix time to insert into DB
		//fmt.Println("unix = ",unix_time) //int64 variable //delete later

		//update DB to insert time of processing current gallery
		query_text = "Update "+client_db+".sfg_attach_gallery_queue set process_time="+strconv.FormatInt(unix_time, 10)+" where sf_login_id="+strconv.Itoa(client_id)
		query_text += " AND item_id="+strconv.Itoa(row[0].Item_id)
		query_text += " AND gallery_id="+strconv.Itoa(row[0].Gallery_id)
		_, err := db.Query(query_text)
		if err != nil {
			fmt.Println("Could not update DB. ", err)
			return
		}
//====================================== vertical and horizontal position of a gallery ======================================//
		text_debug["item_position"] = row[0].Gallery_alignment
		//array with vertical and horizontal alignments of a gallery
		v_h_alignment := strings.Split(row[0].Gallery_alignment, "_")
		//fmt.Println(alignment[0]) //vertical //t=Prepend(top), b=Append(bottom), r=Replace(instead of) //delete later
		//fmt.Println(alignment[1]) //horizontal //center, left, right //delete later
		//fmt.Println(v_h_alignment) //temp // delete later

		var vertical_position map[string]string //where to put new gallery(vertical alignment)
		vertical_position = make(map[string]string)
		vertical_position["t"] = "Prepend"
		vertical_position["b"] = "Append"
		vertical_position["r"] = "Replace"
		//fmt.Println(vertical_position) //delete later

		//sf_login_id := row[0].Sf_login_id //sf_login_id field from DB
		//fmt.Println(sf_login_id)
		//language := row[0].Gallery_language //gallery_language field from DB
		//fmt.Println(language)
//=================================================== GET ITEM DESCRIPTION ==================================================//
		//generate Xml configured toget item description
		getAuctionDescriptionXml := "<?xml version='1.0' encoding='utf-8'?>"
		getAuctionDescriptionXml += "<GetItemRequest xmlns='urn:ebay:apis:eBLBaseComponents'>"
		getAuctionDescriptionXml += "<DetailLevel>ItemReturnDescription</DetailLevel>"
		getAuctionDescriptionXml += "<ErrorLanguage>en_US</ErrorLanguage>"
		getAuctionDescriptionXml += "<WarningLevel>High</WarningLevel>"
		getAuctionDescriptionXml += "<ItemID>"
		getAuctionDescriptionXml += strconv.Itoa(row[0].Item_id)
		getAuctionDescriptionXml += "</ItemID>"
		getAuctionDescriptionXml += "<RequesterCredentials>"
		getAuctionDescriptionXml += "<eBayAuthToken>"
		getAuctionDescriptionXml += user_token
		getAuctionDescriptionXml += "</eBayAuthToken>"
		getAuctionDescriptionXml += "</RequesterCredentials>"
		getAuctionDescriptionXml += "</GetItemRequest>"

		//execute Ebay_api_call function
		eBayItemBodyResponse := Ebay_api_call("GetItem", getAuctionDescriptionXml)
		fmt.Println("HttpRequest GetItem executed with status: ",eBayItemBodyResponse["status"])
		//fmt.Println(eBayItemBodyResponse["items"]) //delete later

//================================== Check if there was an error in executing the function ==================================//
		new_code :=""
		if eBayItemBodyResponse["status"] == "0" {
			//if eBayItemBodyResponse["error_id"] == "" {
			//	eBayItemBodyResponse["error_id"] = "999"
			//}
			query_text = "Update "+client_db+".sfg_attach_gallery_queue set process=0, error="+eBayItemBodyResponse["error_id"]+", error_time="+strconv.FormatInt(unix_time, 10)
			query_text += " where item_id="+strconv.Itoa(row[0].Item_id)+" AND gallery_id="+strconv.Itoa(row[0].Gallery_id)+" AND sf_login_id="+strconv.Itoa(row[0].Sf_login_id)
			_, err = db.Query(query_text)
			if err != nil {
				fmt.Println("Could not update DB after Ebay_api_call error. ", err)
				return
			}
			//fmt.Println("error_id:",eBayItemBodyResponse["error_id"]) delete later
			if eBayItemBodyResponse["error"] != "" {
				fmt.Println("error_body:",eBayItemBodyResponse["error"])
				text_debug["error"] = eBayItemBodyResponse["error"]
			} else if eBayItemBodyResponse["warning"] != "" {
				fmt.Println("warning_body:", eBayItemBodyResponse["warning"])
				text_debug["warning"] = eBayItemBodyResponse["warning"]
			}
			text_debug["first_responce_status"] = eBayItemBodyResponse["status"]
			return true
		} else {
//========================================== Item description received successfully =========================================//
			text_debug["first_responce_status"] = eBayItemBodyResponse["status"]
			//if Ebay_api_call was successful and we have the result (eBayItemBodyResponse["items"])
			auction_description := eBayItemBodyResponse["description"]
			//fmt.Println(auction_description) //delete later
//========================================= if in gallery_option we have delete = 0 =========================================//
			if delete == "false" {
				//generate code
				gallery_option_border_type := gOptions[php_serialize.PhpValue("border_type")].(string)
				new_code = Get_gallery_code(gallery_option_border_type, row, v_h_alignment[1], dbmap) //new_code = gallery_html_code
				//fmt.Println(new_code) //delete later
			}
//======================================== if in gallery_option we have replace = 1 =========================================//
			var alignment_array map[string]int
			alignment_array = make(map[string]int)
			if replace == "true" {
				//first we find old galleries //alignment_array //Find_alignments
				/**
				* TODO: properly import findAlignments function from original script
				*/
				alignment_array = Find_alignments(auction_description, row[0].Sf_login_id, row[0].Gallery_id)
				//then we remove old galleries from description
				if alignment_array["t_left"] > 0 ||  alignment_array["t_center"] > 0 || alignment_array["t_right"] > 0 || alignment_array["b_left"] > 0 || alignment_array["b_center"] > 0 || alignment_array["b_right"] > 0 {
					//modify item description. delete existing galleries
					auction_description = Remove_gallery_code(row[0].Sf_login_id, row[0].Gallery_id, auction_description)
				} else {
					//gallery wasn't found, end process. Also remove old gallery from queue.
					query_text = "Delete from "+client_db+".sfg_attach_gallery_queue where item_id="+strconv.Itoa(row[0].Item_id)+" AND sf_login_id="+strconv.Itoa(row[0].Sf_login_id)+" AND gallery_id="+strconv.Itoa(row[0].Gallery_id)
					_, err = db.Query(query_text)
					if err != nil {
						fmt.Println("Could delete from DB. ", err)
						return
					}
					//TEMP UPDATE INSTEAD OF DELETE
					//once all operations are finished - set gallery status to processed in DB
					/*query_text = "Update "+client_db+".sfg_attach_gallery_queue set process=1 where item_id="+strconv.Itoa(row[0].Item_id)
					_, err = db.Query(query_text)
					if err != nil {
						fmt.Println("Could not update DB. ", err)
						return
					}*/
					return true
				}
			} else {
//======================================== if in gallery_option we have replace = 0 =========================================//
				//get an array of alignments of found galleries in description
				alignment_array = Find_alignments(auction_description, row[0].Sf_login_id, row[0].Gallery_id)
			}
			//insert all alignments into log
			text_debug["t_left"] = strconv.Itoa(alignment_array["t_left"])
			text_debug["t_center"] = strconv.Itoa(alignment_array["t_center"])
			text_debug["t_right"] = strconv.Itoa(alignment_array["t_right"])
			text_debug["b_left"] = strconv.Itoa(alignment_array["b_left"])
			text_debug["b_center"] = strconv.Itoa(alignment_array["b_center"])
			text_debug["b_right"] = strconv.Itoa(alignment_array["b_right"])
//============================================== check for gallery duplicates ===============================================//
			//these variables will be used later. captain
			has_duplicates := "false"
			has_vertical_duplicates := "false"
			old_position := ""

			if alignment_array["t_left"] > 0 ||  alignment_array["t_center"] > 0 || alignment_array["t_right"] > 0 || alignment_array["b_left"] > 0 || alignment_array["b_center"] > 0 || alignment_array["b_right"] > 0 {
				for key, old_alignment := range alignment_array {
					if string(key[0]) == v_h_alignment[0] { //key == v_h_alignment[0]+"_"+v_h_alignment[1]
						if old_alignment > 0 {
							has_duplicates = "true"
							has_vertical_duplicates = "true"
							old_position = key
							break
						}
					}
					if old_alignment > 0 {
						has_duplicates = "true"
					}
				}
			}
			//insert duplicates variables into log
			text_debug["has_duplicates"] = has_duplicates
			text_debug["has_vertical_duplicates"] = has_vertical_duplicates
			//fmt.Println(text_debug) //delete later
//==================================== avoid duplicates is true, duplicates are present =====================================//
			//Remove all duplicates we found
			if Avoid_duplicates == true && has_duplicates == "true"{
				fmt.Println("has duplicate(s). delete duplicates") //delete later
				auction_description = Remove_gallery_code(row[0].Sf_login_id, row[0].Gallery_id, auction_description)
				text_debug["remove_condition"] = "Avoid duplicates, and has duplicates";
			}
//=============================== avoid duplicates is false, vertical duplicates are present ================================//
			if Avoid_duplicates == false && has_vertical_duplicates == "true" {
				// remove all occurrences
				auction_description = Remove_gallery_code(row[0].Sf_login_id, row[0].Gallery_id, auction_description)
				text_debug["remove_condition"] = "Don't avoid duplicates and has vertical duplicates";
				// Add gallery at old position
				for key, old_alignment := range alignment_array {
					if old_alignment > 0 {
						if key != old_position {
							v_h_alignment := strings.Split(key, "_")
							gallery_option_border_type := gOptions[php_serialize.PhpValue("border_type")].(string)
							old_code := Get_gallery_code(gallery_option_border_type, row, v_h_alignment[1], dbmap) //new_code = gallery_html_code

							if strings.Contains(key, "t_") == true {
								auction_description = old_code+auction_description
							} else {
								auction_description = auction_description+old_code
							}
						}
					}
				}
			}
//=================================== description has no gallery duplicates, replace = 0 ====================================//
			html_code := ""
			revise_mode := vertical_position[v_h_alignment[0]]
			if has_duplicates == "false" && replace == "false" {
				html_code = new_code
//================================= no duplicates found and request was to delete gallery ===================================//
				if delete == "true" {
					fmt.Println("nothing to delete") //delete later
					//delete gallery from DB
					query_text = "Delete from "+client_db+".sfg_attach_gallery_queue where sf_login_id="+strconv.Itoa(row[0].Sf_login_id)+" AND item_id="+strconv.Itoa(row[0].Item_id)+" AND gallery_id="+strconv.Itoa(row[0].Gallery_id)+" limit 1"
					_, err = db.Query(query_text)
					if err != nil {
						fmt.Println("Could not delete gallery from DB. ", err)
						return
					}
					//TEMP UPDATE INSTEAD OF DELETE
					//once all operations are finished - set gallery status to processed in DB
					/*query_text = "Update "+client_db+".sfg_attach_gallery_queue set process=1 where item_id="+strconv.Itoa(row[0].Item_id)
					_, err = db.Query(query_text)
					if err != nil {
						fmt.Println("Could not update DB after deleting gallery. ", err)
						return
					}*/
				}
			} else {
//==================================================== every other option ===================================================//
				if Avoid_duplicates == true {
					if has_duplicates == "true" {
						revise_mode = vertical_position["r"]
					} else {
						revise_mode = vertical_position[v_h_alignment[0]]
						html_code = new_code
					}
				} else {
					if has_vertical_duplicates == "true" || replace == "true" {
						revise_mode = vertical_position["r"]
					} else {
						revise_mode = vertical_position[v_h_alignment[0]]
						html_code = new_code
					}
				}
//============================================= if after all html_code is empty =============================================//
				if html_code == "" {
					if v_h_alignment[0] == "t" {
						html_code = new_code+auction_description
					} else {
						html_code = auction_description+new_code
					}
				}
			}
			text_debug["revise_mode"] = revise_mode
//================================================= send ReviseItem request =================================================//
			//generate ReviseItem request
			reviseAuctionDescriptionXml := "<?xml version='1.0' encoding='UTF-8'?>"
			reviseAuctionDescriptionXml += "<ReviseItemRequest xmlns='urn:ebay:apis:eBLBaseComponents'>"
			reviseAuctionDescriptionXml += "<Version>"+Compatibility_level+"</Version>"
			reviseAuctionDescriptionXml += "<ErrorLanguage>en_US</ErrorLanguage>"
			reviseAuctionDescriptionXml += "<WarningLevel>High</WarningLevel>"
			reviseAuctionDescriptionXml += "<Item>"
			reviseAuctionDescriptionXml += "<ItemID>"+strconv.Itoa(row[0].Item_id)+"</ItemID>"
			reviseAuctionDescriptionXml += "<Description><![CDATA["+html_code+"]]></Description>" //<![CDATA['.$htmlCode.']]> //html_code //new text&lt;/br&gt;
			reviseAuctionDescriptionXml += "<DescriptionReviseMode>"+revise_mode+"</DescriptionReviseMode>"
			reviseAuctionDescriptionXml += "</Item>"
			reviseAuctionDescriptionXml += "<RequesterCredentials>"
			reviseAuctionDescriptionXml += "<eBayAuthToken>"+user_token+"</eBayAuthToken>"
			reviseAuctionDescriptionXml += "</RequesterCredentials>"
			reviseAuctionDescriptionXml += "</ReviseItemRequest>"

			//execute Ebay_api_call function
			eBayReviseItemResponse := Ebay_api_call("ReviseItem", reviseAuctionDescriptionXml)
			fmt.Println("HttpRequest ReviseItem executed with status: ",eBayReviseItemResponse["status"])

			text_debug["final_responce_status"] = eBayReviseItemResponse["status"]
			//text_debug["final_responce_items"] = eBayReviseItemResponse["items"]
			//text_debug["final_responce_description"] = eBayReviseItemResponse["description"]
//======================================== if ReviseItem request was NOT successful =========================================//
			if eBayReviseItemResponse["status"] == "0"  {
				//if eBayReviseItemResponse["error_id"] == "" {
				//	eBayReviseItemResponse["error_id"] = "666"
				//}
				query_text = "Update "+client_db+".sfg_attach_gallery_queue set process=0, error="+eBayReviseItemResponse["error_id"]+", error_time="+strconv.FormatInt(unix_time, 10)
				query_text += " where item_id="+strconv.Itoa(row[0].Item_id)+" AND gallery_id="+strconv.Itoa(row[0].Gallery_id)+" AND sf_login_id="+strconv.Itoa(row[0].Sf_login_id)
				_, err = db.Query(query_text)
				if err != nil {
					fmt.Println("Could not update DB after Ebay_api_call error. ", err)
					return
				}
				if eBayItemBodyResponse["error"] != "" {
					fmt.Println("error_body:",eBayItemBodyResponse["error"])
					text_debug["error"] = eBayItemBodyResponse["error"]
				} else if eBayItemBodyResponse["warning"] != "" {
					fmt.Println("warning_body:", eBayItemBodyResponse["warning"])
					text_debug["warning"] = eBayItemBodyResponse["warning"]
				}
				//log
				if Debug == true {
					for key, value := range text_debug {
						if _, err = log_file.WriteString(key+": "+value+"\r\n"); err != nil { fmt.Println("error writing to file 7:", err) }
					}
				}
				return true //original
				//return false
			} else {
//=========================================== if ReviseItem request was successful ==========================================//
				if replace == "true" || delete == "true" {
					if  gOptions[php_serialize.PhpValue("replace")].(int) > 0 {
						//delete gallery from DB
						query_text = "Delete from "+client_db+".sfg_attach_gallery_queue where sf_login_id="+strconv.Itoa(row[0].Sf_login_id)+" AND item_id="+strconv.Itoa(row[0].Item_id)+" AND gallery_id="+strconv.Itoa(row[0].Gallery_id)+" limit 1"
						_, err = db.Query(query_text)
						if err != nil {
							fmt.Println("Could not delete gallery from DB. ", err)
							return
						}
						//TEMP UPDATE INSTEAD OF DELETE
						/*query_text = "Update "+client_db+".sfg_attach_gallery_queue set process=1 where item_id="+strconv.Itoa(row[0].Item_id)
						_, err = db.Query(query_text)
						if err != nil {
							fmt.Println("Could not update DB. ", err)
							return
						}*/
					}
				}
				//log
				if Debug == true {
					for key, value := range text_debug {
						if _, err = log_file.WriteString(key+": "+value+"\r\n"); err != nil { fmt.Println("error writing to file 7:", err) }
					}
				}
			}
			//this code is not needed anymore(?)
			//once all operations are finished - set gallery status to processed in DB
			query_text = "Update "+client_db+".sfg_attach_gallery_queue set process=1 where item_id="+strconv.Itoa(row[0].Item_id)
			_, err = db.Query(query_text)
			if err != nil {
				fmt.Println("Could not update DB. ", err)
				return
			}
			return true
		}
		fmt.Println("Updated DB.")	//delete later
	}
	return true
}
