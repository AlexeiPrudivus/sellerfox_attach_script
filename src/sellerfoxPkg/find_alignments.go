package sellerfoxPkg
import (
//	"fmt"
	"regexp"
	"strings"
	"strconv"
	"encoding/hex"
	"io"
	"crypto/md5"
)

/**
* Finds all occurences of html gallery code
* @param string $description
* @param integer $loginId
* @param integer $galleryId
* @return boolean|multitype:number
*/
func Find_alignments(auction_description string, Sf_login_id, Gallery_id int)(map[string]int){
	/*if auction_description == "" {
		return 0
	}*/

	//create a map containing the amount of found alignments
	var alignment map[string]int
	alignment = make(map[string]int)
	alignment["t_left"] = 0
	alignment["t_center"] = 0
	alignment["t_right"] = 0
	alignment["b_left"] = 0
	alignment["b_center"] = 0
	alignment["b_right"] = 0

	//make sure to restore escaped characters in auction_description
	//fmt.Println(auction_description)
	auction_description = strings.Replace(auction_description, "&#34;", "\"", -1) //"
	auction_description = strings.Replace(auction_description, "&lt;", "<", -1) // <
	auction_description = strings.Replace(auction_description, "&gt;", ">", -1) // >
	auction_description = strings.Replace(auction_description, "&#39;", "'", -1) // '
	auction_description = strings.Replace(auction_description, "&amp;", "&", -1) // &
	//fmt.Println(auction_description)

//=========================================== find galleries in item description ============================================//
	reg := regexp.MustCompile("(value=\"breite=[0-9]{1,}&hoehe=[0-9]{1,}&GNR="+strconv.Itoa(Gallery_id)+"&VKNR="+strconv.Itoa(Sf_login_id)+".*)(<!-- End of Sellerfox Gallery \")([^\"]*)(\" -->)")
	reg_gallery_settings := reg.FindAllStringSubmatch(auction_description, -1)
	//fmt.Printf("%#v\n", reg_gallery_settings) //delete later

	req_gallery_names := make([]string, len(reg_gallery_settings))
	for i:=0;i<len(reg_gallery_settings);i++ {
		req_gallery_names[i] = reg_gallery_settings[i][3]
	}
	//fmt.Println(req_gallery_names) delete later
//================================================ if no galleries are found ================================================//
	if len(req_gallery_names) == 0 {
		return alignment
	}
//============================================ if at least one gallery is found =============================================//
	unique_galleries := Unique_array(req_gallery_names)

		for _, occurrence := range unique_galleries {
			/**
			* TODO: write convert2regex() function from original script
			*/
			//check if there is any text BEFORE gallery. if yes, then vertical alignment is not "t", it is "b"
			reg = regexp.MustCompile("(.*)<!-- Begin of Sellerfox Gallery \""+occurrence+"\" -->")
			reg_before := reg.FindAllStringSubmatch(auction_description, 1)

			/*if reg_before != nil {
				fmt.Println(reg_before[0][0]) //delete later
				fmt.Println(reg_before[0][1]) //delete later
			}*/

			v_alignment := ""
			if reg_before != nil {
				if reg_before[0][1] == "" {
					v_alignment = "t"
				} else {
					v_alignment = "b"
				}
			}
			//then check text-align: value. this value is horizontal alignment
			md5_hash := md5.New()
			io.WriteString(md5_hash, strconv.Itoa(Sf_login_id)+strconv.Itoa(Gallery_id))
			reg = regexp.MustCompile("<div id=\"sfg_"+hex.EncodeToString(md5_hash.Sum(nil))+"\" style='width: 100%; text-align: [a-z]{4,6};'>")
			reg_horizontal := reg.FindStringSubmatch(auction_description)

			//assign horizontal alignment value to variable h_align
			h_alignment := ""
			for i, _ := range reg_horizontal {
				if strings.Contains(reg_horizontal[i], "center") {
					h_alignment = "center"
				} else if strings.Contains(reg_horizontal[i], "left") {
					h_alignment = "left"
				} else if strings.Contains(reg_horizontal[i], "right") {
					h_alignment = "right"
				}
			}
			//having v and h alignments increase the amount of v_h alignment combinations in alignment array by 1
			if h_alignment != "" {
				alignment[v_alignment+"_"+h_alignment]++
			}
		}
	return alignment
}
