package sellerfoxPkg

//============================================= table mirroring structs for gorp ============================================//
type Sf_contract struct {
	Sf_login_id int 		`db:"sf_login_id"`
	Name string				`db:"name"`
	Gekuendiged int			`db:"gekuendiged"`
	Not_attach_auctions int	`db:"not_attach_auctions"`
}

type Sfg_attach_gallery_queue struct {
	Export_id int				`db:"export_id"`
	Sf_login_id int				`db:"sf_login_id"`
	Item_id int					`db:"item_id"`
	Gallery_id int				`db:"gallery_id"`
	Gallery_name string			`db:"gallery_name"`
	Gallery_width int			`db:"gallery_width"`
	Gallery_height int			`db:"gallery_height"`
	Gallery_option string		`db:"gallery_option"`
	Gallery_alignment string	`db:"gallery_alignment"`
	Gallery_language string		`db:"gallery_language"`
	Ebay_token string			`db:"ebay_token"`
	Ebay_token_expire int		`db:"ebay_token_expire"`
	Process int					`db:"process"`
	Process_time int			`db:"process_time"`
	Error int					`db:"error"`
	Error_time int				`db:"error_time"`
}

type Sfg_auctions struct {
	Item_id int	`db:"item_id"`
	Ebay_id int `db:"ebay_id"`
}

type Sfg_settings struct {
	Id int					`db:"id"`
	Ebay_id string			`db:"ebay_id"`
	Ebay_token_expire int	`db:"ebay_token_expire"`
}

type Sfg_gallery struct {
	Id int				`db:"id"`
	Name string			`db:"name"`
	Show_categories int	`db:"show_categories"`
}

type Single_row_gallery_view struct {
	Export_id int
	Sf_login_id int
	Item_id int
	Gallery_id int
	Gallery_name string
	Gallery_width int
	Gallery_height int
	Gallery_option string
	Gallery_alignment string
	Gallery_language string
	Ebay_token string
	Ebay_token_expire int
	Process int
	Process_time int
	Error int
	Error_time int
	Ebay_id string
}
