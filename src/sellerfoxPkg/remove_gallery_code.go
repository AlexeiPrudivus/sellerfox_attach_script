package sellerfoxPkg
import (
	"strconv"
	"regexp"
	"strings"
	"log"
)

func Remove_gallery_code(Sf_login_id, gallery_id int, auction_description string)(string){
	//make sure to restore escaped characters in auction_description
	//fmt.Println(auction_description) //delete later
	auction_description = strings.Replace(auction_description, "&#34;", "\"", -1) //"
	auction_description = strings.Replace(auction_description, "&lt;", "<", -1) // <
	auction_description = strings.Replace(auction_description, "&gt;", ">", -1) // >
	auction_description = strings.Replace(auction_description, "&#39;", "'", -1) // '
	auction_description = strings.Replace(auction_description, "&amp;", "&", -1) // &
	//fmt.Println(auction_description) //delete later

	//find all gallery names here
	reg, err := regexp.Compile("value=\"breite=[0-9]{1,}&hoehe=[0-9]{1,}&GNR="+strconv.Itoa(gallery_id)+"&VKNR="+strconv.Itoa(Sf_login_id)+".*<!-- End of Sellerfox Gallery \"([^\"]*)\" -->")
	if err != nil {
		log.Fatal(err)
	}
	reg_result_array := reg.FindAllStringSubmatch(auction_description, -1)
	//fmt.Printf("%#v\n", reg_result_array) //delete later

	req_gallery_names := make([]string, len(reg_result_array))
	u := 0
	for i, reg_1 := range reg_result_array {
		for j, reg_2 := range reg_1 {
			if j == 1 {
				req_gallery_names[i] = reg_2
				u++
			}
		}
	}
	//fmt.Println("gallery names array:",req_gallery_names) //delete later

	req_gallery_names = Unique_array(req_gallery_names)
	//for each gallery name found delete all galeries with this name
	for _, gallery_name := range req_gallery_names {
		reg, err = regexp.Compile("<!-- Begi[n]{1,2} of Sellerfox Gallery \""+gallery_name+"\" -->.*?<!-- End of Sellerfox Gallery \""+gallery_name+"\" -->")
		if err != nil {
			log.Fatal(err)
		}

		auction_description = reg.ReplaceAllString(auction_description, "")
		//fmt.Println(auction_description__no_gallery) //delete later
	}
	return auction_description
}
