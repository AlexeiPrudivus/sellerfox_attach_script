package sellerfoxPkg

//===================================================== global variables ====================================================//
//switch on debug mode
var Debug bool = false
//debug log file address
var File_address string = "/logs" //"D:/wamp/www/go/sellerfox_attach_script/src/logs"
//opened file
//var file ...

//avoid duplicating of the same gallery in auction's description
var Avoid_duplicates bool = true

//ebay api user token for sandbox
const User_token string = "AgAAAA**AQAAAA**aAAAAA**JvxqVA**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GhDJmLoQ2dj6x9nY+seQ**jB8DAA**AAMAAA**SV08+Y1N3fekdqHMwBas80sXRZ7RCcWnHTq8yBhJH16FhHPP6vBjIVQ3rSyE15u4/mH3etyK1PmHp1jUPxSFw+UrXr6jSrOYvr43VDf43IlMpU1/+3C5t6OsUn7bMgFVWeXzlNN69gks9QJ9VwQ5R9Gfxf87nUwdpG0EaY+Vgoow1DDnCLDZ5F8+Jn6Sm2jiOe3QAKNDq6phX515R7wF169uiKePvTZVF/lD9/tV/JbeJotP8bR8Zl8GOmlrTT5HZZz5pjA7VNsWA1p0KtiBOaTon1BTAYkuPv+JlqYgeEvXdf6eMif/+gipvEZe+BrYxTFHSpdTiSsAWjpoHiVGqJCyAnyzEkV0gE4kJR0rOM2v3/q5d4TThSwjycU1oq3EzcMFSNSFBgeofys18EP0h7va+gmijRCDt5DC2f4ZboJ6W/4i/TgQAiwfF2PlvVorJ/cXalMX2IdwOJX/Dze+aX5srpYmn39irw/W2WriXgSPMo0DM3gF5Mf+SUdBg81xAejGw9FHQSh5ANOT9SbU/zqLkaamtlIyS1MqqpQoXNODr9Y8Lzx2VivdGZI1D1XiVXSxtB/1W7xyWqjTwKeF5Mel+rA6X6ANc/ujx4/uKu7b+1TxJvIowJ3BHpLMZC+sue/tBKds8C23ZaV48NUiIRKGtKakyVWO+P7wHXpYmZsHd/rBDgl9PlXJ54RXpQNFot+6uT4MlcWNDPb7QvRJFcD7rOqLHl+9qMKcMqQg4zpOJWm5V5InRdXcgzi9d1kV"
//version of api
const Compatibility_level string = "899"
//keys for ebay api sandbox
const Dev_id string = "7204ff0d-a607-456e-829a-4255fe9afabe"
const App_id string = "AlexeiPr-19be-4dfc-bb8f-7971e4ac458e"
const Cert_id string = "355dc5ea-2a62-411c-a5d4-0f99a28e4aad"
//ebay api url
const Server_url string = "https://api.sandbox.ebay.com/ws/api.dll" //original/ will give xml
//var Server_url string = "http://cgi.sandbox.ebay.com/ws/eBayISAPI.dll?ViewItem&item=" //new/ not used/ will give html

//0 = usa
var Site_id string = "0"

