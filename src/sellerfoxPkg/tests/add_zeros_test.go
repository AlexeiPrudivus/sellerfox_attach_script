package tests

import (
	"testing"
	"sellerfoxPkg"
)

type test_pair struct {
	in_value int
	out_value string
}

var test_pairs_array = []test_pair{
	{ 5, "00005" },
	{ 15, "00015" },
	{ 315, "00315" },
	{ 2315, "02315" },
	{ 92315, "92315" },
}

//func main() {}

func TestAdd_zeros(t *testing.T) {
	for _, pair := range test_pairs_array {
		v := sellerfoxPkg.Add_zeros(pair.in_value)
		if v != pair.out_value {
			t.Error(
				"For", pair.in_value,
				"expected", pair.out_value,
				"got", v,
			)
		}
	}
	/*//0000X
	db_id_zeroes := prudivusPkg.Add_zeros(5)
	if db_id_zeroes != "00005" {
		t.Error("Expected 00005, got ", db_id_zeroes)
	}
	//000XX
	db_id_zeroes = prudivusPkg.Add_zeros(15)
	if db_id_zeroes != "00015" {
		t.Error("Expected 00015, got ", db_id_zeroes)
	}
	//00XXX
	db_id_zeroes = prudivusPkg.Add_zeros(315)
	if db_id_zeroes != "00315" {
		t.Error("Expected 00315, got ", db_id_zeroes)
	}
	//0XXXX
	db_id_zeroes = prudivusPkg.Add_zeros(2315)
	if db_id_zeroes != "02315" {
		t.Error("Expected 02315, got ", db_id_zeroes)
	}
	//XXXXX
	db_id_zeroes = prudivusPkg.Add_zeros(92315)
	if db_id_zeroes != "92315" {
		t.Error("Expected 92315, got ", db_id_zeroes)
	}*/
}
