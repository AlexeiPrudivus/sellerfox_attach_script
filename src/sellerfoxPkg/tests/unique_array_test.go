package tests

import (
	"testing"
	"fmt"
	"sellerfoxPkg"
)

type test_pair_2 struct {
	in_value []string
	out_value []string
}

var test_pairs_array_2 = []test_pair_2{
	{ []string{"1","1","1","1","1","1","1","1"}, []string{"1"} },
	{ []string{"1","1","2","1","3","1","5","4"}, []string{"1","2","3","5","4"} },
	{ []string{"0","0","0"}, []string{"0"} },
}

func TestUnique_array(t *testing.T) {
	for _, pair := range test_pairs_array_2 {
		slice1 := sellerfoxPkg.Unique_array(pair.in_value)
		slice2 := pair.out_value

		//fmt.Println("in_array:",slice1)
		//fmt.Println("out_array:",slice2)

		strSlice1 := fmt.Sprintf("%#v", slice1)
		strSlice2 := fmt.Sprintf("%#v", slice2)

		if strSlice1 != strSlice2 {
			t.Error("Expected "+strSlice2+", got ", strSlice1)
		}
	}
}

/*func main() {
	array := []string{"2","2","3","2","3","2","4","2","5","5","5","5","5","5","5","5","7896867"}

	u_array := unique_array(array)
	fmt.Println(u_array)
}*/
