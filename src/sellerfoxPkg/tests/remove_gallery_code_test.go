package tests

import (
	"testing"
	"sellerfoxPkg"
	"strings"
)

func TestRemove_gallery_array(t *testing.T) {
	//TEST 1
	in_string  := "&lt;!-- Begin of Sellerfox Gallery &#34;PrudivusGallery&#34; --&gt;&lt;br /&gt;&lt;div id=&#34;sfg_ad007545a1422f2fdd454f31f2eb6229&#34; style=&#39;width: 100%; text-align: center;&#39;&gt;&lt;script type=&#34;text/javascript&#34;&gt;/*ItemID*/a=new Array(&#34;&lt;sc&#34;,&#34;ar SF&#34;,&#34;cume&#34;,&#34;cati&#34;,&#34;/scri&#34;);b=new Array(&#34;ript&gt;v&#34;,&#34;L=do&#34;,&#34;nt.lo&#34;,&#34;on;&lt;&#34;,&#34;pt&gt;&#34;);for(i=0;i&lt;5;i++){document.write(a[i]+b[i]);} var l=SFL;var itemid=0;if(!l.search){l=l.pathname;if(l){var s=l.indexOf(&#34;Z&#34;,&#34;itemZ&#34;);if(l.length&gt;1&amp;&amp;s&gt;-1){var x=l.substring(s+1,s+20);itemid=x.substring(0,x.indexOf(&#34;Q&#34;));}}}else{l=l.search;if(l){var s=l.split(&#34;&amp;&#34;);for(var i=0;i&lt;=s.length;i++){if(s[i]){if(s[i].substring(0,5)==&#39;item=&#39;){var itemid=s[i].substring(5,s[i].length);break;}}}}}/*End of ItemID*//*Gallery*/document.write(&#39;&lt;object classid=&#34;clsid:d27cdb6e-ae6d-11cf-96b8-444553540000&#34; codebase=&#34;http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0&#34; width=&#34;960&#34; height=&#34;830&#34; id=&#34;sellerfox&#34; &gt;&#39;);document.write(&#39;&lt;param name=&#34;movie&#34; value=&#34;http://gallery.sellerfox.de/swf/sf3.swf&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;quality&#34; value=&#34;high&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;scale&#34; value=&#34;noscale&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;salign&#34; value=&#34;lt&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;wmode&#34; value=&#34;transparent&#34; /&gt;&#39;); document.write(&#39;&lt;param name=&#34;bgcolor&#34; value=&#34;#ffffff&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;allowScriptAccess&#34; value=&#34;always&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;FlashVars&#34; value=&#34;breite=960&amp;hoehe=830&amp;GNR=1&amp;VKNR=15272&amp;LANG=en&amp;Itemid=&#39;+itemid+&#39;&#34; /&gt;&#39;);document.write(&#39;&lt;embed src=&#34;http://gallery.sellerfox.de/swf/sf3.swf&#34; quality=&#34;high&#34; scale=&#34;noscale&#34; salign=&#34;lt&#34; wmode=&#34;transparent&#34; bgcolor=&#34;#ffffff&#34; width=&#34;960&#34; height=&#34;830&#34; name=&#34;sellerfox&#34;  FlashVars=&#34;breite=960&amp;hoehe=830&amp;GNR=1&amp;VKNR=15272&amp;LANG=en&amp;Itemid=&#39;+itemid+&#39;&#34; allowScriptAccess = &#34;always&#34; type=&#34;application/x-shockwave-flash&#34; pluginspage=&#34;http://www.macromedia.com/go/getflashplayer&#34; /&gt;&#39;);document.write(&#39;&lt;/object&gt;&#39;);/*End of Gallery*//*Static*/if(true != sellerfoxstatic){document.write(&#39;&lt;img src=&#34;http://static.sellerfox.eu/index.php?page=tracking&amp;v=15272&amp;i=&#39;+itemid+&#39;&#34; height=&#34;1&#34; width=&#34;1&#34; border=&#34;0&#34;&gt;&#39;);var sellerfoxstatic = true;}/*End of Static*/&lt;/script&gt;&lt;/div&gt;&lt;br /&gt;&lt;!-- End of Sellerfox Gallery &#34;PrudivusGallery&#34; --&gt;Description before galleryDescription after gallery"
	in_string = strings.Replace(in_string, "&#34;", "\"", -1) //"
	in_string = strings.Replace(in_string, "&lt;", "<", -1) // <
	in_string = strings.Replace(in_string, "&gt;", ">", -1) // >
	in_string = strings.Replace(in_string, "&#39;", "'", -1) // '
	in_string = strings.Replace(in_string, "&amp;", "&", -1) // &

	in_string = sellerfoxPkg.Remove_gallery_code(15272,1,in_string)
	out_string := "Description before galleryDescription after gallery"

	//fmt.Println(in_string)
	//fmt.Println(out_string)

	if in_string != out_string {
		t.Error("Expected "+in_string+", got ", out_string)
	}

	//TEST 2
	in_string  = "Description before gallery&lt;!-- Begin of Sellerfox Gallery &#34;PrudivusGallery&#34; --&gt;&lt;br /&gt;&lt;div id=&#34;sfg_ad007545a1422f2fdd454f31f2eb6229&#34; style=&#39;width: 100%; text-align: center;&#39;&gt;&lt;script type=&#34;text/javascript&#34;&gt;/*ItemID*/a=new Array(&#34;&lt;sc&#34;,&#34;ar SF&#34;,&#34;cume&#34;,&#34;cati&#34;,&#34;/scri&#34;);b=new Array(&#34;ript&gt;v&#34;,&#34;L=do&#34;,&#34;nt.lo&#34;,&#34;on;&lt;&#34;,&#34;pt&gt;&#34;);for(i=0;i&lt;5;i++){document.write(a[i]+b[i]);} var l=SFL;var itemid=0;if(!l.search){l=l.pathname;if(l){var s=l.indexOf(&#34;Z&#34;,&#34;itemZ&#34;);if(l.length&gt;1&amp;&amp;s&gt;-1){var x=l.substring(s+1,s+20);itemid=x.substring(0,x.indexOf(&#34;Q&#34;));}}}else{l=l.search;if(l){var s=l.split(&#34;&amp;&#34;);for(var i=0;i&lt;=s.length;i++){if(s[i]){if(s[i].substring(0,5)==&#39;item=&#39;){var itemid=s[i].substring(5,s[i].length);break;}}}}}/*End of ItemID*//*Gallery*/document.write(&#39;&lt;object classid=&#34;clsid:d27cdb6e-ae6d-11cf-96b8-444553540000&#34; codebase=&#34;http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0&#34; width=&#34;960&#34; height=&#34;830&#34; id=&#34;sellerfox&#34; &gt;&#39;);document.write(&#39;&lt;param name=&#34;movie&#34; value=&#34;http://gallery.sellerfox.de/swf/sf3.swf&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;quality&#34; value=&#34;high&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;scale&#34; value=&#34;noscale&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;salign&#34; value=&#34;lt&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;wmode&#34; value=&#34;transparent&#34; /&gt;&#39;); document.write(&#39;&lt;param name=&#34;bgcolor&#34; value=&#34;#ffffff&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;allowScriptAccess&#34; value=&#34;always&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;FlashVars&#34; value=&#34;breite=960&amp;hoehe=830&amp;GNR=1&amp;VKNR=15272&amp;LANG=en&amp;Itemid=&#39;+itemid+&#39;&#34; /&gt;&#39;);document.write(&#39;&lt;embed src=&#34;http://gallery.sellerfox.de/swf/sf3.swf&#34; quality=&#34;high&#34; scale=&#34;noscale&#34; salign=&#34;lt&#34; wmode=&#34;transparent&#34; bgcolor=&#34;#ffffff&#34; width=&#34;960&#34; height=&#34;830&#34; name=&#34;sellerfox&#34;  FlashVars=&#34;breite=960&amp;hoehe=830&amp;GNR=1&amp;VKNR=15272&amp;LANG=en&amp;Itemid=&#39;+itemid+&#39;&#34; allowScriptAccess = &#34;always&#34; type=&#34;application/x-shockwave-flash&#34; pluginspage=&#34;http://www.macromedia.com/go/getflashplayer&#34; /&gt;&#39;);document.write(&#39;&lt;/object&gt;&#39;);/*End of Gallery*//*Static*/if(true != sellerfoxstatic){document.write(&#39;&lt;img src=&#34;http://static.sellerfox.eu/index.php?page=tracking&amp;v=15272&amp;i=&#39;+itemid+&#39;&#34; height=&#34;1&#34; width=&#34;1&#34; border=&#34;0&#34;&gt;&#39;);var sellerfoxstatic = true;}/*End of Static*/&lt;/script&gt;&lt;/div&gt;&lt;br /&gt;&lt;!-- End of Sellerfox Gallery &#34;PrudivusGallery&#34; --&gt;Description after gallery"
	in_string = strings.Replace(in_string, "&#34;", "\"", -1) //"
	in_string = strings.Replace(in_string, "&lt;", "<", -1) // <
	in_string = strings.Replace(in_string, "&gt;", ">", -1) // >
	in_string = strings.Replace(in_string, "&#39;", "'", -1) // '
	in_string = strings.Replace(in_string, "&amp;", "&", -1) // &

	in_string = sellerfoxPkg.Remove_gallery_code(15272,1,in_string)
	out_string = "Description before galleryDescription after gallery"

	//fmt.Println(in_string)
	//fmt.Println(out_string)

	if in_string != out_string {
		t.Error("Expected "+in_string+", got ", out_string)
	}

	//TEST 3
	in_string  = "Description before gallery&lt;!-- Beginn of Sellerfox Gallery &#34;PrudivusGallery&#34; --&gt;&lt;br /&gt;&lt;div id=&#34;sfg_ad007545a1422f2fdd454f31f2eb6229&#34; style=&#39;width: 100%; text-align: center;&#39;&gt;&lt;script type=&#34;text/javascript&#34;&gt;/*ItemID*/a=new Array(&#34;&lt;sc&#34;,&#34;ar SF&#34;,&#34;cume&#34;,&#34;cati&#34;,&#34;/scri&#34;);b=new Array(&#34;ript&gt;v&#34;,&#34;L=do&#34;,&#34;nt.lo&#34;,&#34;on;&lt;&#34;,&#34;pt&gt;&#34;);for(i=0;i&lt;5;i++){document.write(a[i]+b[i]);} var l=SFL;var itemid=0;if(!l.search){l=l.pathname;if(l){var s=l.indexOf(&#34;Z&#34;,&#34;itemZ&#34;);if(l.length&gt;1&amp;&amp;s&gt;-1){var x=l.substring(s+1,s+20);itemid=x.substring(0,x.indexOf(&#34;Q&#34;));}}}else{l=l.search;if(l){var s=l.split(&#34;&amp;&#34;);for(var i=0;i&lt;=s.length;i++){if(s[i]){if(s[i].substring(0,5)==&#39;item=&#39;){var itemid=s[i].substring(5,s[i].length);break;}}}}}/*End of ItemID*//*Gallery*/document.write(&#39;&lt;object classid=&#34;clsid:d27cdb6e-ae6d-11cf-96b8-444553540000&#34; codebase=&#34;http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0&#34; width=&#34;960&#34; height=&#34;830&#34; id=&#34;sellerfox&#34; &gt;&#39;);document.write(&#39;&lt;param name=&#34;movie&#34; value=&#34;http://gallery.sellerfox.de/swf/sf3.swf&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;quality&#34; value=&#34;high&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;scale&#34; value=&#34;noscale&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;salign&#34; value=&#34;lt&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;wmode&#34; value=&#34;transparent&#34; /&gt;&#39;); document.write(&#39;&lt;param name=&#34;bgcolor&#34; value=&#34;#ffffff&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;allowScriptAccess&#34; value=&#34;always&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;FlashVars&#34; value=&#34;breite=960&amp;hoehe=830&amp;GNR=1&amp;VKNR=15272&amp;LANG=en&amp;Itemid=&#39;+itemid+&#39;&#34; /&gt;&#39;);document.write(&#39;&lt;embed src=&#34;http://gallery.sellerfox.de/swf/sf3.swf&#34; quality=&#34;high&#34; scale=&#34;noscale&#34; salign=&#34;lt&#34; wmode=&#34;transparent&#34; bgcolor=&#34;#ffffff&#34; width=&#34;960&#34; height=&#34;830&#34; name=&#34;sellerfox&#34;  FlashVars=&#34;breite=960&amp;hoehe=830&amp;GNR=1&amp;VKNR=15272&amp;LANG=en&amp;Itemid=&#39;+itemid+&#39;&#34; allowScriptAccess = &#34;always&#34; type=&#34;application/x-shockwave-flash&#34; pluginspage=&#34;http://www.macromedia.com/go/getflashplayer&#34; /&gt;&#39;);document.write(&#39;&lt;/object&gt;&#39;);/*End of Gallery*//*Static*/if(true != sellerfoxstatic){document.write(&#39;&lt;img src=&#34;http://static.sellerfox.eu/index.php?page=tracking&amp;v=15272&amp;i=&#39;+itemid+&#39;&#34; height=&#34;1&#34; width=&#34;1&#34; border=&#34;0&#34;&gt;&#39;);var sellerfoxstatic = true;}/*End of Static*/&lt;/script&gt;&lt;/div&gt;&lt;br /&gt;&lt;!-- End of Sellerfox Gallery &#34;PrudivusGallery&#34; --&gt;Description after gallery&lt;!-- Begin of Sellerfox Gallery &#34;PrudivusGallery&#34; --&gt;&lt;br /&gt;&lt;div id=&#34;sfg_ad007545a1422f2fdd454f31f2eb6229&#34; style=&#39;width: 100%; text-align: center;&#39;&gt;&lt;script type=&#34;text/javascript&#34;&gt;/*ItemID*/a=new Array(&#34;&lt;sc&#34;,&#34;ar SF&#34;,&#34;cume&#34;,&#34;cati&#34;,&#34;/scri&#34;);b=new Array(&#34;ript&gt;v&#34;,&#34;L=do&#34;,&#34;nt.lo&#34;,&#34;on;&lt;&#34;,&#34;pt&gt;&#34;);for(i=0;i&lt;5;i++){document.write(a[i]+b[i]);} var l=SFL;var itemid=0;if(!l.search){l=l.pathname;if(l){var s=l.indexOf(&#34;Z&#34;,&#34;itemZ&#34;);if(l.length&gt;1&amp;&amp;s&gt;-1){var x=l.substring(s+1,s+20);itemid=x.substring(0,x.indexOf(&#34;Q&#34;));}}}else{l=l.search;if(l){var s=l.split(&#34;&amp;&#34;);for(var i=0;i&lt;=s.length;i++){if(s[i]){if(s[i].substring(0,5)==&#39;item=&#39;){var itemid=s[i].substring(5,s[i].length);break;}}}}}/*End of ItemID*//*Gallery*/document.write(&#39;&lt;object classid=&#34;clsid:d27cdb6e-ae6d-11cf-96b8-444553540000&#34; codebase=&#34;http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0&#34; width=&#34;960&#34; height=&#34;830&#34; id=&#34;sellerfox&#34; &gt;&#39;);document.write(&#39;&lt;param name=&#34;movie&#34; value=&#34;http://gallery.sellerfox.de/swf/sf3.swf&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;quality&#34; value=&#34;high&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;scale&#34; value=&#34;noscale&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;salign&#34; value=&#34;lt&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;wmode&#34; value=&#34;transparent&#34; /&gt;&#39;); document.write(&#39;&lt;param name=&#34;bgcolor&#34; value=&#34;#ffffff&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;allowScriptAccess&#34; value=&#34;always&#34; /&gt;&#39;);document.write(&#39;&lt;param name=&#34;FlashVars&#34; value=&#34;breite=960&amp;hoehe=830&amp;GNR=1&amp;VKNR=15272&amp;LANG=en&amp;Itemid=&#39;+itemid+&#39;&#34; /&gt;&#39;);document.write(&#39;&lt;embed src=&#34;http://gallery.sellerfox.de/swf/sf3.swf&#34; quality=&#34;high&#34; scale=&#34;noscale&#34; salign=&#34;lt&#34; wmode=&#34;transparent&#34; bgcolor=&#34;#ffffff&#34; width=&#34;960&#34; height=&#34;830&#34; name=&#34;sellerfox&#34;  FlashVars=&#34;breite=960&amp;hoehe=830&amp;GNR=1&amp;VKNR=15272&amp;LANG=en&amp;Itemid=&#39;+itemid+&#39;&#34; allowScriptAccess = &#34;always&#34; type=&#34;application/x-shockwave-flash&#34; pluginspage=&#34;http://www.macromedia.com/go/getflashplayer&#34; /&gt;&#39;);document.write(&#39;&lt;/object&gt;&#39;);/*End of Gallery*//*Static*/if(true != sellerfoxstatic){document.write(&#39;&lt;img src=&#34;http://static.sellerfox.eu/index.php?page=tracking&amp;v=15272&amp;i=&#39;+itemid+&#39;&#34; height=&#34;1&#34; width=&#34;1&#34; border=&#34;0&#34;&gt;&#39;);var sellerfoxstatic = true;}/*End of Static*/&lt;/script&gt;&lt;/div&gt;&lt;br /&gt;&lt;!-- End of Sellerfox Gallery &#34;PrudivusGallery&#34; --&gt; Description after third gallery"
	in_string = strings.Replace(in_string, "&#34;", "\"", -1) //"
	in_string = strings.Replace(in_string, "&lt;", "<", -1) // <
	in_string = strings.Replace(in_string, "&gt;", ">", -1) // >
	in_string = strings.Replace(in_string, "&#39;", "'", -1) // '
	in_string = strings.Replace(in_string, "&amp;", "&", -1) // &

	in_string = sellerfoxPkg.Remove_gallery_code(15272,1,in_string)
	out_string = "Description before galleryDescription after gallery Description after third gallery"

	//fmt.Println(in_string)
	//fmt.Println(out_string)

	if in_string != out_string {
		t.Error("Expected "+in_string+", got ", out_string)
	}

	//TEST 4
	//empty gallery. only tags
	/*in_string  = "text &lt;!-- Begin of Sellerfox Gallery &#34;PrudivusGallery&#34; --&gt;value=&#34;breite=960&amp;hoehe=830&amp;GNR=1&amp;VKNR=15272&amp;LANG=en&amp;Itemid=&#39;+itemid+&#39;&#34; /&gt&lt;!-- End of Sellerfox Gallery &#34;PrudivusGallery&#34; --&gt;"
	in_string = strings.Replace(in_string, "&#34;", "\"", -1) //"
	in_string = strings.Replace(in_string, "&lt;", "<", -1) // <
	in_string = strings.Replace(in_string, "&gt;", ">", -1) // >
	in_string = strings.Replace(in_string, "&#39;", "'", -1) // '
	in_string = strings.Replace(in_string, "&amp;", "&", -1) // &

	in_string = sellerfoxPkg.Remove_gallery_code(15272,1,in_string)
	out_string = "text "

	//fmt.Println(in_string)
	//fmt.Println(out_string)

	if in_string != out_string {
		t.Error("Expected "+in_string+", got ", out_string)
	}*/
}
