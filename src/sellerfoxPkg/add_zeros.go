package sellerfoxPkg
import (
	"strconv"
)

//func main() {}

//this function adds zeroes to the left side of input parameter so that it's length becomes 5 symbols
func Add_zeros(db_id int)(string_plus_zeroes string){
	R :=""

	if db_id / 10 == 0 {
		R = "0000" + strconv.Itoa(db_id)
	} else
	if db_id / 100 == 0 {
		R = "000" + strconv.Itoa(db_id)
	} else
	if db_id / 1000 == 0 {
		R = "00" + strconv.Itoa(db_id)
	} else
	if db_id / 10000 == 0 {
			R = "0" + strconv.Itoa(db_id)
	} else {
		R += strconv.Itoa(db_id)
	}
	return R
}
